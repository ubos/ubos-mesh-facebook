//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.importer.facebook.on20210706.apps_and_websites_off_of_facebook;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.google.gson.stream.JsonReader;
import java.io.IOException;
import java.text.ParseException;
import net.ubos.importer.ImporterUtils;
import net.ubos.importer.facebook.AbstractSkipTopFbJsonHandler;
import net.ubos.importer.facebook.FbHandlerContext;
import net.ubos.importer.facebook.FbUtils;
import net.ubos.mesh.MeshObject;
import net.ubos.meshbase.history.EditableHistoryMeshBase;
import net.ubos.model.Facebook.FacebookSubjectArea;
import net.ubos.model.primitives.EnumeratedValue;
import net.ubos.model.primitives.IntegerValue;
import net.ubos.model.primitives.StringValue;
import net.ubos.model.primitives.TimeStampValue;

/**
 *
 */
public class YourOffFacebookActivityHandler
    extends
        AbstractSkipTopFbJsonHandler
{
    /**
     * Constructor.
     *
     * @param insideZipFilePattern the path inside of the overall ZIP file that this importer matches
     * @param expectedTopLevelElement the expected element in the top-level JsonObject that we enter
     */
    public YourOffFacebookActivityHandler(
            String insideZipFilePattern,
            String expectedTopLevelElement )
    {
        super( insideZipFilePattern, expectedTopLevelElement );
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected double jsonReaderImportBelow(
            JsonReader       jr,
            MeshObject       hereObject,
            FbHandlerContext context )
        throws
            ParseException,
            IOException
    {
        final String PARTNER_PREFIX = "partners";
        final String EVENT_PREFIX   = "event";
        final String MYSTERY_PREFIX = "mystery";

        EditableHistoryMeshBase mb = context.getMeshBase();

//        mb.executeAt(   context.getTimeOfExport(),
//                        () -> {
                            jr.beginArray();

                            while( jr.hasNext() ) {
                                JsonObject activity = (JsonObject) JsonParser.parseReader( jr );

                                String    name        = activity.get( "name" ).getAsString();
                                JsonArray eventsArray = (JsonArray) activity.get( "events" );

                                MeshObject partnerObj = mb.createMeshObject(
                                        mb.createMeshObjectIdentifierBelow( PARTNER_PREFIX, name ),
                                        FacebookSubjectArea.FACEBOOKPARTNER );
                                partnerObj.setPropertyValue( FacebookSubjectArea.FACEBOOKPARTNER_NAME, StringValue.create( name ));

                                for( int j=0 ; j<eventsArray.size() ; ++j ) {
                                    JsonObject event = (JsonObject) eventsArray.get( j );

                                    long            id   = event.get( "id" ).getAsLong();
                                    TimeStampValue  when = FbUtils.obtainTimestamp( event.get( "timestamp" ).getAsJsonPrimitive() );
                                    EnumeratedValue type = FacebookSubjectArea.FACEBOOKPARTNEREVENT_TYPE_type.select( event.get( "type" ).getAsString() );

                                    MeshObject eventObj = ImporterUtils.createWithoutConflict(
                                            mb.createMeshObjectIdentifierBelow(
                                                    mb.createMeshObjectIdentifierBelow( partnerObj.getIdentifier(), EVENT_PREFIX ),
                                                    String.valueOf( when.getAsMillis() )),
                                            FacebookSubjectArea.FACEBOOKPARTNEREVENT,
                                            mb );

                                    eventObj.setPropertyValue( FacebookSubjectArea.FACEBOOKPARTNEREVENT_TYPE, type );
                                    eventObj.setPropertyValue( FacebookSubjectArea.FACEBOOKPARTNEREVENT_WHEN, when );

                                    MeshObject mysteryObject = ImporterUtils.findOrCreate(
                                            mb.createMeshObjectIdentifierBelow( MYSTERY_PREFIX, String.valueOf( id )),
                                            FacebookSubjectArea.FACEBOOKPARTNEREVENTUNKNOWN,
                                            mb );
                                    mysteryObject.setPropertyValue( FacebookSubjectArea.FACEBOOKPARTNEREVENTUNKNOWN_ID, IntegerValue.create( id ));

                                    eventObj.blessRole( FacebookSubjectArea.FACEBOOKPARTNEREVENT_IDENTIFIES_FACEBOOKPARTNEREVENTUNKNOWN_S, mysteryObject );

                                    eventObj.blessRole( FacebookSubjectArea.FACEBOOKPARTNEREVENT_OCCURSAT_FACEBOOKPARTNER_S, partnerObj );
                                }
                            }

                            jr.endArray();
//                        });

        return PERFECT;
    }
}
