//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.importer.facebook.on20200106;

import java.io.File;
import java.io.IOException;
import java.util.regex.Pattern;
import java.util.zip.ZipFile;
import net.ubos.importer.facebook.FbHandlerContext;
import net.ubos.importer.facebook.ObjectDirectory;
import net.ubos.importer.facebook.on20200106.about_you.FriendPeerGroupHandler;
import net.ubos.importer.facebook.on20200106.ads.AdsInterestsHandler;
import net.ubos.importer.facebook.on20200106.ads.AdvertisersWhoUploadedAContactListWithYourInformationHandler;
import net.ubos.importer.facebook.on20200106.ads.AdvertisersYouveInteractedWithHandler;
import net.ubos.importer.facebook.on20200106.apps_and_websites.AppsAndWebsitesHandler;
import net.ubos.importer.facebook.on20200106.apps_and_websites.YourAppsHandler;
import net.ubos.importer.facebook.on20200106.comments.CommentsHandler;
import net.ubos.importer.facebook.on20200106.following_and_followers.FollowersHandler;
import net.ubos.importer.facebook.on20200106.following_and_followers.FollowingHandler;
import net.ubos.importer.facebook.on20200106.friends.FriendsHandler;
import net.ubos.importer.facebook.on20200106.messages.MessagesHandler;
import net.ubos.importer.facebook.on20200106.photos_and_videos.AlbumHandler;
import net.ubos.importer.facebook.on20200106.photos_and_videos.FbTypedBlobImporterHandler;
import net.ubos.importer.facebook.on20200106.photos_and_videos.YourVideosHandler;
import net.ubos.importer.facebook.on20200106.posts.OtherPeoplesPostsToYourTimelineHandler;
import net.ubos.importer.facebook.on20200106.posts.YourPostsHandler;
import net.ubos.importer.facebook.on20200106.profile_information.ProfileInformationHandler;
import net.ubos.importer.facebook.on20200106.saved_items_and_collections.SavedItemsAndCollectionsHandler;
import net.ubos.importer.facebook.on20200106.search_history.YourSearchHistoryHandler;
import net.ubos.importer.handler.AbstractMultiHandlerImporter;
import net.ubos.importer.handler.ImporterHandler;
import net.ubos.importer.handler.ImporterHandlerContext;
import net.ubos.importer.handler.ImporterHandlerNode;
import net.ubos.importer.handler.directory.SkipIntermediateDirectoryImporterHandler;
import net.ubos.importer.handler.file.DefaultFileImporterHandler;
import net.ubos.importer.handler.json.DefaultJsonImporterHandler;
import net.ubos.importer.handler.remainders.RateRemaindersImporterHandler;
import net.ubos.importer.handler.zip.DefaultZipImporterHandler;
import net.ubos.importer.handler.zip.NoOpZipDirectoryImporterHandler;
import net.ubos.mesh.namespace.MeshObjectIdentifierNamespace;
import net.ubos.meshbase.history.EditableHistoryMeshBase;
import net.ubos.model.Facebook.FacebookSubjectArea;
import net.ubos.util.logging.Log;

/**
 * Imports a Facebook data export ZIP file.
 */
public class FacebookImporter20200106
    extends
        AbstractMultiHandlerImporter
{
    private static final Log log = Log.getLogInstance(FacebookImporter20200106.class );

    /**
     * Constructor.
     */
    public FacebookImporter20200106()
    {
        super( "Facebook importer (format as of 2020-01-06)", HANDLERS, RATE_REMAINDERS_HANDLER );
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected ImporterHandlerContext setupImport(
            ImporterHandlerNode     rootNode,
            String                  defaultIdNamespace,
            EditableHistoryMeshBase mb )
        throws
            IOException
    {
        if( defaultIdNamespace != null ) {
            MeshObjectIdentifierNamespace thisNs = mb.getDefaultNamespace();
            thisNs.addExternalName( defaultIdNamespace );
            thisNs.setPreferredExternalName( defaultIdNamespace );
        }

        ObjectDirectory dir = new ObjectDirectory( mb );

        File topFile = rootNode.getFile();
        if( topFile.isDirectory() ) {
            return new FbHandlerContext.Directory( rootNode.getTimeOfExport(), mb, dir, topFile );

        } else {
            ZipFile zipFile = new ZipFile( topFile );
            return new FbHandlerContext.Zip( rootNode.getTimeOfExport(), mb, dir, zipFile );
        }
    }

    /**
     * Name of the file that contains the expected entry patterns.
     */
    public static final String EXPECTED_ENTRY_PATTERNS_FILE = "ExpectedEntryPatterns.txt";

    /**
     * The ImporterContentHandlers for this Importer
     */
    public static final ImporterHandler [] HANDLERS = {
        // Structure first
            new SkipIntermediateDirectoryImporterHandler(), // support unpacked directories, too
            new DefaultZipImporterHandler(),

            // we need information about ourselves first

            new ProfileInformationHandler(
                    "profile_information/profile_information.json", "profile" ),

            // make sure the media objects are there when we look for them

            new FbTypedBlobImporterHandler(
                    "photos_and_videos/.*\\.(gif|jpg|mp4|png)" ),
            new DefaultFileImporterHandler(
                    "messages/.*\\.(docx|pdf|txt)", ImporterHandler.SUFFICIENT ),

            new FriendPeerGroupHandler(
                    "about_you/friend_peer_group.json", "friend_peer_group" ),

            new AdsInterestsHandler(
                    "ads/ads_interests.json", "topics" ),
            new AdvertisersWhoUploadedAContactListWithYourInformationHandler(
                    "ads/advertisers_who_uploaded_a_contact_list_with_your_information.json", "custom_audiences" ),
            new AdvertisersYouveInteractedWithHandler(
                    "ads/advertisers_you've_interacted_with.json", "history" ),

            // first the apps, then the posts from them

            new AppsAndWebsitesHandler(
                    "apps_and_websites/apps_and_websites.json", "installed_apps" ),
            new YourAppsHandler(
                    "apps_and_websites/your_apps.json", "admined_apps" ),

            new CommentsHandler(
                    "comments/comments.json", "comments" ),

                            // events/event_invitations.json
                            // events/your_event_responses.json

                            // following_and_followers/followed_pages.json
            new FollowersHandler(
                    "following_and_followers/followers.json", "followers" ),
            new FollowingHandler(
                    "following_and_followers/following.json", "following" ),
                            // following_and_followers/unfollowed_pages.json

            new FriendsHandler(
                    "friends/friends.json", "friends" ),
                            // friends/rejhected_friend_requests.json
                            // friends/removed_friends.json
                            // friends/sent_friend_requests.json

                            // groups/your_group_membership_activity.json
                            // groups/your_posts_and_comments_in_groups.json

                            // likes_and_reactions/likes_on_external_sites.json
                            // likes_and_reactions/pages.json
                            // likes_and_reactions/posts_and_comments.json

                            // location/ ???

                            // marketplace/ ???

            new MessagesHandler( "messages/archived_threads/.*/message_1.json", FacebookSubjectArea.FACEBOOKMESSAGETHREAD_CATEGORY_type_ARCHIVED ),
            new MessagesHandler( "messages/filtered_threads/.*/message_1.json", FacebookSubjectArea.FACEBOOKMESSAGETHREAD_CATEGORY_type_FILTERED ),
            new MessagesHandler( "messages/inbox/.*/message_1.json",            FacebookSubjectArea.FACEBOOKMESSAGETHREAD_CATEGORY_type_INBOX ),
            new MessagesHandler( "messages/message_requests/.*/message_1.json", FacebookSubjectArea.FACEBOOKMESSAGETHREAD_CATEGORY_type_REQUESTS ),


                            // other_activity/polls_you_voted_on.json

                            // pages/your_pages.json

                            // payment_history/payment_history.json

            new AlbumHandler(
                    "photos_and_videos/album/\\d+.json" ),
            new YourVideosHandler(
                    "photos_and_videos/your_videos.json", "videos" ),

                            // portal/ ???

            new OtherPeoplesPostsToYourTimelineHandler(
                    "posts/other_people's_posts_to_your_timeline.json" ),
            new YourPostsHandler(
                    "posts/your_posts_\\d+.json" ),

                            // profile_information/profile_update_history.json

            new SavedItemsAndCollectionsHandler(
                    "saved_items_and_collections/saved_items_and_collections.json", "saves_and_collections" ),

            new YourSearchHistoryHandler(
                    "search_history/your_search_history.json", "searches" ),

                            // security_and_login_information/account_activity.json
                            // security_and_login_information/administrative_records.json
                            // security_and_login_information/authorized_logins.json
                            // security_and_login_information/datr_cookie_info.json
                            // security_and_login_information/login_protection_data.json
                            // security_and_login_information/logins_and_logouts.json
                            // security_and_login_information/used_ip_addresses.json
                            // security_and_login_information/where_you're_logged_in.json

                            // stories/archived_stories.json

                            // your_places/places_you've_created.json

    // Fallback

            new NoOpZipDirectoryImporterHandler( ImporterHandler.UNRATED, true ),

            new DefaultFileImporterHandler( ".*\\.(docx|gif|jpg|mp4|pdf|png|txt)", ImporterHandler.UNRATED ),
            new DefaultJsonImporterHandler( ImporterHandler.FALLBACK ),
    };

    public static final RateRemaindersImporterHandler RATE_REMAINDERS_HANDLER;
    static {
        RateRemaindersImporterHandler handler = null;
        try {
            handler = new RateRemaindersImporterHandler(
                        FacebookImporter20200106.class.getResourceAsStream( EXPECTED_ENTRY_PATTERNS_FILE ),
                        ImporterHandler.SUFFICIENT, // we aren't actually parsing, but that's intentionally so
                        ImporterHandler.MAYBE_WRONG );

        } catch( IOException ex ) {
            log.error( ex );
        }
        RATE_REMAINDERS_HANDLER = handler;
    }

    /**
     * The filename pattern for a Facebook export file.
     */
    public static final Pattern FACEBOOK_EXPORT_FILENAME_PATTERN = Pattern.compile( "\\.zip$" );
}
