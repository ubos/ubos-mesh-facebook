//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.importer.facebook;

import com.google.gson.stream.JsonReader;
import com.google.gson.stream.MalformedJsonException;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.StringReader;
import java.nio.charset.StandardCharsets;
import java.text.ParseException;
import java.util.regex.Pattern;
import net.ubos.importer.handler.AbstractBasicFileImporterHandler;
import net.ubos.importer.handler.ImporterHandlerContext;
import net.ubos.importer.handler.ImporterHandlerNode;
import net.ubos.importer.handler.UnexpectedContentException;
import net.ubos.mesh.MeshObject;
import net.ubos.util.StreamUtils;

/**
 * Superclass of our ImporterHandlers that read JSON.
 * One important job this class has it is to fix broken JSON binary encoding that's exported by Facebook.
 */
public abstract class AbstractFbJsonHandler
    extends
        AbstractBasicFileImporterHandler
{
    /**
     * Constructor.
     *
     * @param insideZipFilePattern the path inside of the overall ZIP file that this importer matches
     */
    protected AbstractFbJsonHandler(
            String insideZipFilePattern )
    {
        super( Pattern.compile( "[^/]+/" + insideZipFilePattern ), PERFECT );
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public double doImport(
            ImporterHandlerNode    toBeImported,
            ImporterHandlerContext context )
        throws
            UnexpectedContentException,
            ParseException,
            IOException
    {
        if( !toBeImported.canGetStream()) {
            return IMPOSSIBLE;
        }

        double ret;

        try {
            InputStream in = toBeImported.createStream();

            String broken = StreamUtils.slurp( new InputStreamReader( in, StandardCharsets.UTF_8 ));
            String fixed  = FbUtils.fixBrokenCharEncoding( broken );

            ret = jsonReaderImport(
                    new JsonReader( new StringReader( fixed )),
                    toBeImported.getHereMeshObject(),
                    (FbHandlerContext) context );

        } catch( MalformedJsonException ex ) {
            ret = IMPOSSIBLE;
        }
        return ret;
    }

    /**
     * Knows how to import JSON from a JsonReader.
     *
     * @param jr the to-be imported stream
     * @param hereObject the here MeshObject
     * @param context the context for the importing
     * @return the score
     * @throws UnexpectedContentException the ImporterHandler was not prepared for the content it found
     * @throws ParseException a parsing error occurred
     * @throws IOException an I/O project occurred
     */
    protected abstract double jsonReaderImport(
            JsonReader       jr,
            MeshObject       hereObject,
            FbHandlerContext context )
        throws
            UnexpectedContentException,
            ParseException,
            IOException;
}
