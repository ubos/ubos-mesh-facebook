//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.importer.facebook.on20200106.friends;

import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.google.gson.stream.JsonReader;
import java.io.IOException;
import java.text.ParseException;
import net.ubos.importer.facebook.AbstractSkipTopFbJsonHandler;
import net.ubos.importer.facebook.FbHandlerContext;
import net.ubos.importer.facebook.ObjectDirectory;
import net.ubos.mesh.MeshObject;
import net.ubos.mesh.MeshObjectIdentifier;
import net.ubos.meshbase.history.EditableHistoryMeshBase;
import net.ubos.model.Identity.IdentitySubjectArea;
import net.ubos.model.Social.SocialSubjectArea;
import net.ubos.model.primitives.StringValue;

public class FriendsHandler
    extends
        AbstractSkipTopFbJsonHandler
{
    /**
     * Constructor.
     *
     * @param insideZipFilePattern the path inside of the overall ZIP file that this importer matches
     * @param expectedTopLevelElement the expected element in the top-level JsonObject that we enter
     */
    public FriendsHandler(
            String insideZipFilePattern,
            String expectedTopLevelElement )
    {
        super( insideZipFilePattern, expectedTopLevelElement );
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected double jsonReaderImportBelow(
            JsonReader       jr,
            MeshObject       hereObject,
            FbHandlerContext context )
        throws
            ParseException,
            IOException
    {
        double ret = PERFECT;

        EditableHistoryMeshBase mb  = context.getMeshBase();
        ObjectDirectory         dir = context.getObjectDirectory();

//        mb.executeAt(   context.getTimeOfExport(),
//                        () -> {
                            mb.deleteMeshObject( hereObject );

                            jr.beginArray();
                            while( jr.hasNext() ) {
                                JsonObject current = (JsonObject) JsonParser.parseReader( jr );

                                String name      = current.get( "name" ).getAsString();
                                // long   timestamp = current.get( "timestamp" ).getAsLong();
                                String email     = current.get( "contact_info" ) != null ? current.get( "contact_info" ).getAsString() : null;

                                MeshObject           friendAccount          = dir.obtainByName( name, mb );
                                MeshObjectIdentifier friendAccountProfileId = mb.createMeshObjectIdentifierBelow(
                                        friendAccount.getIdentifier(),
                                        "profile" );

                                MeshObject friendAccountProfile = mb.findMeshObjectByIdentifier( friendAccountProfileId );
                                // may exist already if I have multiple friends with the same name
                                if( friendAccountProfile == null ) {
                                    friendAccountProfile = mb.createMeshObject(
                                            friendAccountProfileId,
                                            IdentitySubjectArea.INDIVIDUALACCOUNTPROFILE );
                                }

                                if( friendAccountProfile.getPropertyValue( IdentitySubjectArea.INDIVIDUALACCOUNTPROFILE_FULLNAME ) == null ) {
                                    friendAccountProfile.setPropertyValue( IdentitySubjectArea.INDIVIDUALACCOUNTPROFILE_FULLNAME, StringValue.createOrNull( name ));
                                }
                                if( friendAccountProfile.getPropertyValue( IdentitySubjectArea.ACCOUNTPROFILE_EMAILADDRESS ) == null ) {
                                    friendAccountProfile.setPropertyValue( IdentitySubjectArea.ACCOUNTPROFILE_EMAILADDRESS, StringValue.createOrNull( email ));
                                }

                                if( !friendAccountProfile.isRelated( IdentitySubjectArea.ACCOUNTPROFILE_FOR_ACCOUNT_S, friendAccount )) {
                                    friendAccountProfile.blessRole( IdentitySubjectArea.ACCOUNTPROFILE_FOR_ACCOUNT_S, friendAccount );
                                }
                                if( !mb.getHomeObject().isRelated( SocialSubjectArea.ACCOUNT_ISFRIENDSWITH_ACCOUNT_U, friendAccount ) ) {
                                    mb.getHomeObject().blessRole( SocialSubjectArea.ACCOUNT_ISFRIENDSWITH_ACCOUNT_U, friendAccount );
                                }
                            }
                            jr.endArray();
//                        });

        return ret;
    }
}
