//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.importer.facebook;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.google.gson.JsonPrimitive;
import java.net.MalformedURLException;
import java.net.URL;
import java.nio.charset.StandardCharsets;
import java.text.ParseException;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import net.ubos.mesh.AbstractMeshObjectIdentifierFactory;
import net.ubos.mesh.MeshObject;
import net.ubos.mesh.MeshObjectIdentifier;
import net.ubos.meshbase.MeshBase;
import net.ubos.meshbase.MeshBaseView;
import net.ubos.model.Facebook.FacebookSubjectArea;
import net.ubos.model.ThreadedNotes.ThreadedNotesSubjectArea;
import net.ubos.model.Web.WebSubjectArea;
import net.ubos.model.primitives.TimeStampValue;
import net.ubos.util.Pair;
import net.ubos.util.logging.Log;

/**
 * Useful utility functions.
 */
public abstract class FbUtils
{
    private static final Log log = Log.getLogInstance( FbUtils.class );

    /**
     * Keep this abstract.
     */
    private FbUtils() {}

    /**
     * Process a data JSON fragment that contains the content of a post and
     * some last updated info.
     *
     * @param json the JSON fragment that contains the "data" node
     * @param postObj the MeshObject representing the post
     */
    public static void processDataPostLastUpdated(
            JsonObject json,
            MeshObject postObj )
    {
        if( !json.has( "data" )) {
            return;
        }

        String         post          = null;
        TimeStampValue lastUpdatedOn = null;

        JsonArray dataJson = json.getAsJsonArray( "data" );
        for( int j=0 ; j<dataJson.size() ; ++j ) {
            JsonObject currentDataJson = (JsonObject) dataJson.get( j );
            if( currentDataJson.has( "post" )) {
                if( post == null ) {
                    post = currentDataJson.get( "post" ).getAsString();
                } else {
                    log.warn( "Have post already:", post, currentDataJson.get( "post" ).getAsString());
                }
            }
            if( currentDataJson.has( "update_timestamp" )) {
                if( lastUpdatedOn == null ) {
                    lastUpdatedOn = obtainTimestamp( currentDataJson.getAsJsonPrimitive( "update_timestamp" ));
                } else {
                    log.warn( "Have update_timestamp already:", lastUpdatedOn, currentDataJson.get( "update_timestamp" ).getAsString());
                }
            }
        }

        postObj.setPropertyValue( FacebookSubjectArea.FACEBOOKPOST_LASTUPDATEDWHEN, lastUpdatedOn );
        if( post != null ) {
            postObj.setPropertyValue(
                    ThreadedNotesSubjectArea.NOTE_CONTENT,
                    ThreadedNotesSubjectArea.NOTE_CONTENT_type.createPlainTextBlobValue( post ));
        }
    }

    /**
     * Process an attachments JSON fragment that contains external contexts or media.
     *
     * @param json the JSON fragment that contains the "attachments" node
     * @param postObj the MeshObject representing the post
     * @throws ParseException thrown if a MeshObjectIdentifier could not be parsed
     */
    public static void processAttachments(
            JsonObject json,
            MeshObject postObj )
        throws
            ParseException
    {
        if( !json.has( "attachments" )) {
            return;
        }
        JsonArray attachmentsArrayJson = json.get( "attachments" ).getAsJsonArray();
        MeshBase  mb = postObj.getMeshBase();

        // apparently it can be empty, and it can contain more than one entry like
        // "place" in addition to "media"
        for( int i=0 ; i<attachmentsArrayJson.size() ; ++i ) {
            JsonArray dataArrayJson = ((JsonObject)attachmentsArrayJson.get( i )).get( "data" ).getAsJsonArray();
            for( int j=0 ; j<dataArrayJson.size() ; ++j ) {
                JsonObject dataObjectJson = dataArrayJson.get( j ).getAsJsonObject();
                if( dataObjectJson.has( "external_context" )) {
                    JsonObject externalContextJson = dataObjectJson.getAsJsonObject( "external_context" );
                    if( externalContextJson.has( "url" )) {
                        String url = externalContextJson.getAsJsonPrimitive( "url" ).getAsString();

                        try {
                            // This should be:
                            //
                            // MeshObject external = theMeshBase.accessLocally( url );
                            //
                            // but for now:

                            String withoutProtocol;
                            if( url.equals( "facebook.com" )) { // apparently that gets to have no protocol
                                withoutProtocol = url;

                            } else {
                                StringBuilder buf = new StringBuilder();
                                URL u = new URL( url );
                                buf.append( u.getHost());
                                buf.append( u.getPath());
                                if( u.getQuery() != null ) {
                                    buf.append( "?" );
                                    buf.append( u.getQuery() );
                                }
                                withoutProtocol = buf.toString();
                                if( withoutProtocol.endsWith( "/" )) {
                                    withoutProtocol = withoutProtocol.substring( 0, withoutProtocol.length()-1 );
                                }
                            }
                            withoutProtocol = removeHttpProtocol( withoutProtocol ); // for now

                            MeshObject external = mb.findMeshObjectByIdentifier( withoutProtocol );
                            if( external == null ) {
                                external = mb.createMeshObject( withoutProtocol, WebSubjectArea.WEBRESOURCE );
                                // its own namespace?
                            }

                            postObj.blessRole( ThreadedNotesSubjectArea.NOTE_INRESPONSETO_ANY_S, external );

                        } catch( MalformedURLException ex ) {
                            log.warn( ex );
                        }
                    }
                }
                if( dataObjectJson.has( "media" )) {
                    // FIXME
                }
                if( dataObjectJson.has( "place" )) {
                    // FIXME
                }
            }
        }
    }

    /**
     * Helper to parse the title of a post sent to me and determine the sender of
     * the post.
     *
     * @param title the post title
     * @return the sender Meshobject
     * @throws ParseException thrown if a name could not be parsed into a MeshObjectIdentifier
     */
    public static MeshObject parseSentPostTitle(
            String          title,
            Pattern []      patterns,
            ObjectDirectory dir,
            MeshBase        mbv )
        throws
            ParseException
    {
        for( Pattern pattern : patterns ) {
            Matcher m = pattern.matcher( title );
            if( m.matches() ) {
                String senderName = m.group( 1 );

                MeshObject sender = dir.obtainByName( senderName, mbv );

                return sender;
            }
        }
        return null;
    }

    /**
     * Helper to parse a comment title and determine the sender and receiver of
     * the comment.
     *
     * @param title the comment title
     * @return Pair of sender, receiver (which may be the same)
     * @throws ParseException thrown if a name could not be parsed into a MeshObjectIdentifier
     */
    public static Pair<MeshObject,MeshObject> parseCommentTitle(
            String          title,
            Pattern []      commentOtherPatterns,
            Pattern []      commentSelfPatterns,
            ObjectDirectory dir,
            MeshBase        mb )
        throws
            ParseException
    {
        for( Pattern pattern : commentOtherPatterns ) {
            Matcher m = pattern.matcher( title );
            if( m.matches() ) {
                String senderName   = m.group( 1 );
                if( m.groupCount() > 1 ) {
                    String receiverName = m.group( 2 );

                    return new Pair<>( dir.obtainByName( senderName, mb ), dir.obtainByName( receiverName, mb ));
                } else {
                    return new Pair<>( dir.obtainByName( senderName, mb ), null );
                }
            }
        }
        for( Pattern pattern : commentSelfPatterns ) {
            Matcher m = pattern.matcher( title );
            if( m.matches() ) {
                String selfName = m.group( 1 );

                MeshObject self = dir.obtainByName( selfName, mb );

                return new Pair<>( self, self );
            }
        }
        return null;
    }


    public static void handleMediaComments(
            JsonArray            commentsJson,
            MeshObjectIdentifier mediaObjId,
            ObjectDirectory      dir,
            MeshBase             mbv )
        throws
            ParseException
    {
        // FIXME
//        // apparently the comments on videos are exported differently
//
//         for( int j=0 ; j<commentsJson.size() ; ++j ) {
//            JsonObject commentJson = (JsonObject) commentsJson.get( j );
//
//            TimeStampValue commentTimestamp = obtainTimestamp( commentJson.getAsJsonPrimitive( "timestamp" ));
//            String         comment          = commentJson.getAsJsonPrimitive( "comment" ).getAsString();
//
//            mbv.getMeshBase().ex
//            MeshObject commentObj = mbv.createMeshObject(
//                    mbv.createMeshObjectIdentifierBelow(
//                            mbv.createMeshObjectIdentifierBelow( mediaObj.getIdentifier(), "comment" ),
//                            String.valueOf( commentTimestamp.getAsMillis())),
//                    FacebookSubjectArea.FACEBOOKCOMMENT );
//            commentObj.setPropertyValue(
//                    ThreadedNotesSubjectArea.NOTE_CONTENT,
//                    ThreadedNotesSubjectArea.NOTE_CONTENT_type.createPlainTextBlobValue( comment ));
//
//            commentObj.blessRole( ThreadedNotesSubjectArea.NOTE_INRESPONSETO_ANY_S, mediaObj );
//
//            if( commentJson.has( "author" )) {
//                // apparently not always
//                String         author           = commentJson.getAsJsonPrimitive( "author" ).getAsString();
//                MeshObject     authorObj        = dir.obtainByName( author, mbv );
//                commentObj.blessRole( ThreadedNotesSubjectArea.NOTE_CREATEDBY_ACCOUNT_S, authorObj );
//            }
//        }
    }

    /**
     * Fix the broken UTF character encoding.
     *
     * @param broken the broken String
     * @return the fixed string
     */
    public static String fixBrokenCharEncoding(
            String broken )
    {
        StringBuilder ret = new StringBuilder( broken.length() );

        int   index = 0;

        while( true ) {
            int index2 = findBrokenPattern( broken, index );
            if( index2 == -1 ) {
                // we are done
                ret.append( broken.substring( index ));
                break;
            }
            ret.append( broken.substring( index, index2 ));

            // subsequent marked strings need to be appended
            // they are 6 chars each: \u0000
            final int GROUPLEN = 6;
            StringBuilder buf = new StringBuilder();
            int index3 = index2; // for debugging
            while( true ) {
                buf.append( broken.substring( index3, index3 + GROUPLEN ));
                index3 += GROUPLEN;
                if( findBrokenPattern( broken, index3 ) != index3 ) {
                    // decode and append
                    byte [] raw = new byte[ buf.length() / GROUPLEN ];
                    for( int i=0 ; i<raw.length ; ++i ) {
                        raw[i] = FbUtils.parseHexTwo( buf, i*GROUPLEN + 4 );
                    }
                    String decodedGroup = new String( raw, StandardCharsets.UTF_8 );
                    ret.append( decodedGroup );
                    break;
                }
            }

            index = index3;
        }
        return ret.toString();
    }

    /**
     * Determine the index where the broken pattern occurs again next.
     *
     * @param broken the broken String
     * @param index where to start looking from
     * @return the index, or -1
     */
    public static int findBrokenPattern(
            String broken,
            int    index )
    {
        final String MARKER = "\\u";

        int current = index;
        while( true ) {
            int current2 = broken.indexOf( MARKER, current );
            if( current2 == -1 ) {
                return -1;
            }
            if( current2 + 5 > broken.length() ) {
                // incomplete
                return -1;
            }
            if(    broken.charAt( current2 + 2 ) == '0'
                && broken.charAt( current2 + 3 ) == '0'
                && FbUtils.isHex( broken.charAt( current2 + 4 ))
                && FbUtils.isHex( broken.charAt( current2 + 5 )))
            {
                return current2;
            }
            current = current2 + 1;
        }
    }

    /**
     * Is this character a hex number?
     *
     * @param c
     * @return
     */
    public static boolean isHex(
            char c )
    {
        return    ( c >= '0' && c <= '9' )
               || ( c >= 'a' && c <= 'f' )
               || ( c >= 'A' && c <= 'F' );
    }

    /**
     * Helper to parse two chars representing a hex number at this specified index in the String.
     *
     * @param s the String
     * @param start the start index
     * @return the byte
     */
    public static byte parseHexTwo(
            StringBuilder s,
            int           start )
    {
        byte ret = 0;
        char c1  = s.charAt( start );
        char c2  = s.charAt( start+1 );

        ret += parseHex( c1 ) * 16;
        ret += parseHex( c2 );

        return ret;
    }

    /**
     * Helper to parse one char into a hex number.
     *
     * @param c the char
     * @return its hex value
     */
    public static byte parseHex(
            char c )
    {
        int ret;
        if( c >= '0' && c <= '9' ) {
            ret = c - '0';
        } else if( c >= 'a' && c <= 'f' ) {
            ret = c - 'a' + 10;
        } else if( c >= 'A' && c <= 'F' ) {
            ret = c - 'A' + 10;
        } else {
            throw new IllegalArgumentException( "Unexpected char in hex: " + c );
        }
        return (byte) ret;
    }

    /**
     * Remove any leading http:// https:// prefix.
     *
     * @param s the string
     * @return the string without the prefix
     */
    public static String removeHttpProtocol(
            String s )
    {
        for( String prefix : new String[] { "http://", "https://" } ) {
            if( s.startsWith( prefix )) {
                return s.substring( prefix.length() );
            }
        }
        return s;
    }

    /**
     * Create a MeshObjectIdentifier below a localId, by processing path separators and blanks
     * in the identifier String sanely and tolerantly. Or so we hope.
     *
     * @param first the localId below which the MeshObjectIdentifier shall be created
     * @param second the second part of the id
     * @param mbv the MeshBase to use
     * @return the MeshObjectIdentifier
     * @throws ParseException thrown if parsing was unsuccessful
     */
    public static MeshObjectIdentifier identifierBelow(
            String       first,
            String       second,
            MeshBaseView mbv )
        throws
            ParseException
    {
        // trim and forget trailing slashes
        while( second.endsWith( AbstractMeshObjectIdentifierFactory.PATH_SEPARATOR )) {
            second = second.substring( 0, second.length()-1 );
        }
        // compact all white space
        second = second.replaceAll( "\\s+", " " );

        int sep = second.indexOf( AbstractMeshObjectIdentifierFactory.PATH_SEPARATOR );
        MeshObjectIdentifier ret;

        if( sep >= 0 ) {
            ret = mbv.createMeshObjectIdentifierBelow(
                    first,
                    second.substring( 0, sep ),
                    second.substring( sep+1 ).split( "/" ));
        } else {
            ret = mbv.createMeshObjectIdentifierBelow(
                    first,
                    second );
        }

        return ret;
    }

    /**
     * Helper method to obtain a TimeStampValue from a epoch timestamp in the JSON.
     *
     * @param prim the JSONPrimitive
     * @return the TimeStampValue
     */
    public static TimeStampValue obtainTimestamp(
            JsonPrimitive prim )
    {
        // Different parts of the Facebook export use different units:
        // * Most appear to use epoch seconds
        // * But messages appear to use epoch milliseconds
        // Which creates some interesting situations :-)
        // So we take a guess: 10,000 years from the epoch or thereabouts appears to be a good cutoff date
        // Ever had a year 11,970 problem? :-P

        final long cutoff = 10000 * 365 * 24 * 60 * 60L;

        long data = prim.getAsLong();
        if( data > cutoff ) {
            return TimeStampValue.create( data );
        } else {
            return TimeStampValue.createFromEpochSeconds( data );
        }
    }

    /**
     * The prefix for the preferred names of the namespaces that we allocate for Facebook users.
     */
    public static final String USER_NAMESPACE_PREFIX = "facebook.com/user/";
}
