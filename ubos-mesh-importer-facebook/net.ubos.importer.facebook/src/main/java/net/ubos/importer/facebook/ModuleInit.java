//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.importer.facebook;

import net.ubos.importer.Importer;
import net.ubos.importer.facebook.on20200106.FacebookImporter20200106;
import net.ubos.importer.facebook.on20210706.FacebookImporter20210706;
import org.diet4j.core.Module;
import org.diet4j.core.ModuleActivationException;

/**
 * Activate this Module and return an Importer
 */
public class ModuleInit
{
    /**
     * Diet4j module activation.
     *
     * @param thisModule the Module being activated
     * @return the Importer instances
     * @throws ModuleActivationException thrown if module activation failed
     */
    public static Importer [] moduleActivate(
            Module thisModule )
        throws
            ModuleActivationException
    {
        return new Importer[] {
            new FacebookImporter20200106(),
            new FacebookImporter20210706()
        };
    }
}
