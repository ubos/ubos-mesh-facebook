//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.importer.facebook.on20200106.profile_information;

import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.google.gson.stream.JsonReader;
import java.io.IOException;
import java.text.ParseException;
import java.util.Calendar;
import java.util.TimeZone;
import net.ubos.importer.facebook.AbstractSkipTopFbJsonHandler;
import net.ubos.importer.facebook.FbHandlerContext;
import net.ubos.importer.facebook.FbUtils;
import net.ubos.importer.facebook.ObjectDirectory;
import net.ubos.importer.handler.NonblessedImporterHandler;
import net.ubos.importer.handler.UnexpectedContentException;
import net.ubos.mesh.MeshObject;
import net.ubos.mesh.namespace.MeshObjectIdentifierNamespace;
import net.ubos.meshbase.history.EditableHistoryMeshBase;
import net.ubos.model.Facebook.FacebookSubjectArea;
import net.ubos.model.Identity.IdentitySubjectArea;
import net.ubos.model.primitives.EnumeratedValue;
import net.ubos.model.primitives.StringValue;
import net.ubos.model.primitives.TimeStampValue;

public class ProfileInformationHandler
    extends
        AbstractSkipTopFbJsonHandler
{
    /**
     * Constructor.
     *
     * @param insideZipFilePattern the path inside of the overall ZIP file that this importer matches
     * @param expectedTopLevelElement the expected element in the top-level JsonObject that we enter
     */
    public ProfileInformationHandler(
            String insideZipFilePattern,
            String expectedTopLevelElement )
    {
        super( insideZipFilePattern, expectedTopLevelElement );
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected double jsonReaderImportBelow(
            JsonReader       jr,
            MeshObject       hereObject,
            FbHandlerContext context )
        throws
            UnexpectedContentException,
            ParseException,
            IOException
    {
        double ret = PERFECT;

        EditableHistoryMeshBase mb  = context.getMeshBase();
        ObjectDirectory         dir = context.getObjectDirectory();

//        mb.executeAt(   context.getTimeOfExport(),
//                        () -> {
                            mb.deleteMeshObject( hereObject );

                            JsonObject profileJson     = (JsonObject ) JsonParser.parseReader( jr );
                            JsonObject nameJson        = profileJson.getAsJsonObject( "name" );
                            JsonObject birthdayJson    = profileJson.getAsJsonObject( "birthday" );
                            JsonObject genderJson      = profileJson.getAsJsonObject( "gender" );
                            JsonObject currentCityJson = profileJson.getAsJsonObject( "current_city" );
                            String     profileUrl      = profileJson.getAsJsonPrimitive( "profile_uri" ).getAsString();

                            String fullName   = nameJson.getAsJsonPrimitive( "full_name" ).getAsString();
                            String firstName  = nameJson.getAsJsonPrimitive( "first_name" ).getAsString();
                            String middleName = nameJson.getAsJsonPrimitive( "middle_name" ).getAsString();
                            String lastName   = nameJson.getAsJsonPrimitive( "last_name" ).getAsString();

                            Calendar dateOfBirth = Calendar.getInstance();
                            dateOfBirth.setTimeZone( TimeZone.getTimeZone( "America/Los_Angeles" )); // FIXME
                            dateOfBirth.set(
                                    birthdayJson.getAsJsonPrimitive( "year"  ).getAsInt(),
                                    birthdayJson.getAsJsonPrimitive( "month" ).getAsInt() - 1, // month starts with 0
                                    birthdayJson.getAsJsonPrimitive( "day"   ).getAsInt() );

                            String gender = genderJson.getAsJsonPrimitive( "gender_option" ).getAsString();
                            EnumeratedValue genderValue = null;
                            if( "MALE".equalsIgnoreCase( gender )) {
                                genderValue = IdentitySubjectArea.INDIVIDUALACCOUNTPROFILE_GENDER_type_MALE;
                            } else if( "FEMALE".equalsIgnoreCase( gender )) {
                                genderValue = IdentitySubjectArea.INDIVIDUALACCOUNTPROFILE_GENDER_type_FEMALE;
                            } else {
                                throw new UnexpectedContentException( "Unexpected gender:" + gender );
                            }

                            String city = currentCityJson.getAsJsonPrimitive( "name" ).getAsString();

                            // Let's not go through the obtainByName here because we know it doesn't exist yet and we have
                            // full profile information

                            MeshObject myAccount = mb.getHomeObject();
                            myAccount.bless( FacebookSubjectArea.FACEBOOKACCOUNT );
                            myAccount.deleteAttribute( NonblessedImporterHandler.IMPORTER_NAME_ATTRIBUTE );

                            dir.setMainUser( fullName );

                            MeshObject newAccountProfile = mb.createMeshObject(
                                    "profile",
                                    FacebookSubjectArea.FACEBOOKACCOUNTPROFILE );

                            newAccountProfile.setPropertyValue( IdentitySubjectArea.ACCOUNTPROFILE_CITY, StringValue.create( city ));
                            newAccountProfile.setPropertyValue( IdentitySubjectArea.INDIVIDUALACCOUNTPROFILE_FULLNAME,    StringValue.create( fullName ));
                            newAccountProfile.setPropertyValue( IdentitySubjectArea.INDIVIDUALACCOUNTPROFILE_FIRSTNAME,   StringValue.create( firstName ));
                            newAccountProfile.setPropertyValue( IdentitySubjectArea.INDIVIDUALACCOUNTPROFILE_MIDDLENAME,  StringValue.create( middleName ));
                            newAccountProfile.setPropertyValue( IdentitySubjectArea.INDIVIDUALACCOUNTPROFILE_LASTNAME,    StringValue.create( lastName ));
                            newAccountProfile.setPropertyValue( IdentitySubjectArea.INDIVIDUALACCOUNTPROFILE_DATEOFBIRTH, TimeStampValue.create( dateOfBirth ));
                            newAccountProfile.setPropertyValue( IdentitySubjectArea.INDIVIDUALACCOUNTPROFILE_GENDER,      genderValue );
                            newAccountProfile.setPropertyValue( FacebookSubjectArea.FACEBOOKACCOUNTPROFILE_PROFILEURL,    StringValue.create( profileUrl ));

                            newAccountProfile.blessRole( IdentitySubjectArea.ACCOUNTPROFILE_FOR_ACCOUNT_S, myAccount );

                            MeshObjectIdentifierNamespace thisNs = mb.getDefaultNamespace();
                            thisNs.addExternalName( FbUtils.USER_NAMESPACE_PREFIX + fullName );
                            thisNs.setPreferredExternalName( FbUtils.USER_NAMESPACE_PREFIX + fullName );
//                        });

        return ret;
    }
}
