//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.importer.facebook.on20200106.apps_and_websites;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.google.gson.stream.JsonReader;
import java.io.IOException;
import java.text.ParseException;
import net.ubos.importer.ImporterUtils;
import net.ubos.importer.facebook.AbstractSkipTopFbJsonHandler;
import net.ubos.importer.facebook.FbHandlerContext;
import net.ubos.importer.facebook.FbUtils;
import net.ubos.mesh.MeshObject;
import net.ubos.meshbase.history.EditableHistoryMeshBase;
import net.ubos.model.Facebook.FacebookSubjectArea;
import net.ubos.model.primitives.StringValue;
import net.ubos.model.primitives.TimeStampValue;

public class YourAppsHandler
    extends
        AbstractSkipTopFbJsonHandler
{
    /**
     * Constructor.
     *
     * @param insideZipFilePattern the path inside of the overall ZIP file that this importer matches
     * @param expectedTopLevelElement the expected element in the top-level JsonObject that we enter
     */
    public YourAppsHandler(
            String insideZipFilePattern,
            String expectedTopLevelElement )
    {
        super( insideZipFilePattern, expectedTopLevelElement );
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected double jsonReaderImportBelow(
            JsonReader       jr,
            MeshObject       hereObject,
            FbHandlerContext context )
        throws
            ParseException,
            IOException
    {
        double ret = PERFECT;

        EditableHistoryMeshBase mb = context.getMeshBase();

//        mb.executeAt(   context.getTimeOfExport(),
//                        () -> {
                            mb.deleteMeshObject( hereObject );
//                        });

        JsonArray appsArray = (JsonArray) JsonParser.parseReader( jr );

        for( int i=0 ; i<appsArray.size() ; ++i ) {
            JsonObject app = (JsonObject) appsArray.get( i );

            String         name      = app.get( "name" ).getAsString();
            TimeStampValue addedWhen = FbUtils.obtainTimestamp( app.get( "added_timestamp" ).getAsJsonPrimitive() );

//            mb.executeAt(   addedWhen.getAsMillis(),
//                            () -> {
                                MeshObject appObj = ImporterUtils.findOrCreate(
                                        FbUtils.identifierBelow( "apps", name, mb ),
                                        FacebookSubjectArea.FACEBOOKAPP,
                                        mb );

                                appObj.setPropertyValue( FacebookSubjectArea.FACEBOOKAPP_NAME, StringValue.create( name ));

                                // FIXME addedWhen

                                mb.getHomeObject().blessRole( FacebookSubjectArea.FACEBOOKACCOUNT_ISADMINISTRATORFOR_FACEBOOKAPP_S, appObj );
//                            });

        }
        return ret;
    }
}
