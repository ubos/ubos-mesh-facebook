//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.importer.facebook.on20200106.saved_items_and_collections;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.google.gson.JsonPrimitive;
import com.google.gson.stream.JsonReader;
import java.io.IOException;
import java.text.ParseException;
import net.ubos.importer.ImporterUtils;
import net.ubos.importer.facebook.AbstractSkipTopFbJsonHandler;
import net.ubos.importer.facebook.FbHandlerContext;
import net.ubos.importer.facebook.FbUtils;
import net.ubos.importer.handler.UnexpectedContentException;
import net.ubos.mesh.MeshObject;
import net.ubos.meshbase.history.EditableHistoryMeshBase;
import net.ubos.model.Bookmark.BookmarkSubjectArea;
import net.ubos.model.Facebook.FacebookSubjectArea;
import net.ubos.model.Structure.StructureSubjectArea;
import net.ubos.model.Web.WebSubjectArea;
import net.ubos.model.primitives.SelectableMimeType;
import net.ubos.model.primitives.StringValue;
import net.ubos.model.primitives.TimeStampValue;

public class SavedItemsAndCollectionsHandler
    extends
        AbstractSkipTopFbJsonHandler
{
    /**
     * Constructor.
     *
     * @param insideZipFilePattern the path inside of the overall ZIP file that this importer matches
     * @param expectedTopLevelElement the expected element in the top-level JsonObject that we enter
     */
    public SavedItemsAndCollectionsHandler(
            String insideZipFilePattern,
            String expectedTopLevelElement )
    {
        super( insideZipFilePattern, expectedTopLevelElement );
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected double jsonReaderImportBelow(
            JsonReader       jr,
            MeshObject       hereObject,
            FbHandlerContext context )
        throws
            UnexpectedContentException,
            ParseException,
            IOException
    {
        double ret = PERFECT;

        EditableHistoryMeshBase mb = context.getMeshBase();

//        mb.executeAt(   context.getTimeOfExport(),
//                        () -> {
                            mb.deleteMeshObject( hereObject );

                            jr.beginArray();

                            while( jr.hasNext() ) {
                                JsonObject item = (JsonObject) JsonParser.parseReader( jr );

                                TimeStampValue when = FbUtils.obtainTimestamp( item.get( "timestamp" ).getAsJsonPrimitive());
                                String         title = item.get( "title" ).getAsString();

                                MeshObject bookmark = ImporterUtils.createWithoutConflict(
                                        mb.createMeshObjectIdentifierBelow(
                                                "bookmarks",
                                                String.valueOf( when.getAsMillis() )), // not necessarily unique -- maybe they flatten a composite event into two, not sure
                                        FacebookSubjectArea.FACEBOOKBOOKMARK,
                                        mb );
                                bookmark.setPropertyValue( BookmarkSubjectArea.BOOKMARK_DESCRIPTION, BookmarkSubjectArea.BOOKMARK_DESCRIPTION_type.createBlobValue( title, SelectableMimeType.TEXT_PLAIN.getMimeType() ));
                                bookmark.setPropertyValue( BookmarkSubjectArea.BOOKMARK_CREATEDWHEN, when );

                                JsonArray attachmentsArray = (JsonArray) item.get( "attachments" );
                                if( attachmentsArray != null && attachmentsArray.size() == 1 ) {
                                    // Saves of content on Facebook does not have any context info.
                                    JsonArray dataArray = (JsonArray) attachmentsArray.get( 0 ).getAsJsonObject().get( "data" );
                                    if( dataArray == null || dataArray.size() != 1 ) {
                                        throw new UnexpectedContentException( "Failed to find data" );
                                    }
                                    JsonObject dataElement     = (JsonObject) dataArray.get( 0 );
                                    JsonObject externalContext = (JsonObject) dataElement.get( "external_context" );

                                    JsonPrimitive name   = (JsonPrimitive) externalContext.get( "name" );
                                    JsonPrimitive source = (JsonPrimitive) externalContext.get( "source" );
                                    JsonPrimitive url    = (JsonPrimitive) externalContext.get( "url" );

                                    // the Facebook export names for data fields look confused; we try to unconfuse them
                                    String realUrl = url.getAsString();
                                    if( source != null ) {
                                        String realSource = source.getAsString();
                                        if( realSource.startsWith( "http" )) {
                                            realUrl = realSource;
                                        }
                                    }

                                    realUrl = FbUtils.removeHttpProtocol( realUrl ); // for now

                                    if( name != null ) {
                                        bookmark.setPropertyValue( BookmarkSubjectArea.BOOKMARK_TITLE, StringValue.create( name.getAsString() ));
                                    }

                                    MeshObject bookmarked = ImporterUtils.findOrCreate( realUrl, WebSubjectArea.WEBRESOURCE, mb );
                                    bookmark.blessRole( BookmarkSubjectArea.BOOKMARK_REFERENCES_ANY_S, bookmarked );
                                }

                                MeshObject savesAndCollections = ImporterUtils.findOrCreate(
                                        "saves-and-collections",
                                        FacebookSubjectArea.FACEBOOKBOOKMARKCOLLECTION,
                                        mb );
                                savesAndCollections.blessRole( StructureSubjectArea.COLLECTION_COLLECTS_ANY_S, bookmark );

                                // FIXME relate to my account
                            }

                            jr.endArray();
//                        });

        return ret;
    }
}
