//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.importer.facebook.on20200106.ads;

import com.google.gson.JsonArray;
import com.google.gson.JsonParser;
import com.google.gson.stream.JsonReader;
import java.io.IOException;
import java.text.ParseException;
import net.ubos.importer.facebook.AbstractSkipTopFbJsonHandler;
import net.ubos.importer.facebook.FbHandlerContext;
import net.ubos.importer.facebook.FbUtils;
import net.ubos.mesh.MeshObject;
import net.ubos.meshbase.history.EditableHistoryMeshBase;
import net.ubos.model.Facebook.FacebookSubjectArea;
import net.ubos.model.Marketing.MarketingSubjectArea;
import net.ubos.model.primitives.StringValue;

public class AdsInterestsHandler
    extends
        AbstractSkipTopFbJsonHandler
{
    /**
     * Constructor.
     *
     * @param insideZipFilePattern the path inside of the overall ZIP file that this importer matches
     * @param expectedTopLevelElement the expected element in the top-level JsonObject that we enter
     */
    public AdsInterestsHandler(
            String insideZipFilePattern,
            String expectedTopLevelElement )
    {
        super( insideZipFilePattern, expectedTopLevelElement );
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected double jsonReaderImportBelow(
            JsonReader       jr,
            MeshObject       hereObject,
            FbHandlerContext context )
        throws
            ParseException,
            IOException
    {
        double ret = PERFECT;

        EditableHistoryMeshBase mb = context.getMeshBase();

        JsonArray topicsArray = (JsonArray) JsonParser.parseReader( jr );

//        mb.executeAt(   context.getTimeOfExport(),
//                        () -> {
                            mb.deleteMeshObject( hereObject );

                            MeshObject interestCollection = null; // created as needed

                            for( int i=0 ; i<topicsArray.size() ; ++i ) {
                                String topicString = topicsArray.get( i ).getAsString();

                                MeshObject interest = mb.createMeshObject(
                                        FbUtils.identifierBelow( "interest", topicString, mb ),
                                        FacebookSubjectArea.FACEBOOKINTEREST );
                                interest.setPropertyValue( MarketingSubjectArea.INTEREST_NAME, StringValue.create( topicString ));

                                if( interestCollection == null ) {
                                    interestCollection = mb.createMeshObject(
                                            "interests",
                                            FacebookSubjectArea.FACEBOOKINTERESTCOLLECTION );

                                    mb.getHomeObject().blessRole( MarketingSubjectArea.ACCOUNT_HASASSOCIATED_MARKETINGINFO_S, interestCollection );
                                }
                                interestCollection.blessRole( MarketingSubjectArea.INTERESTCOLLECTION_COLLECTS_INTEREST_S, interest );
                            }
//                        });

        return ret;
    }
}
