//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.importer.facebook.on20200106.posts;

import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.google.gson.stream.JsonReader;
import java.io.IOException;
import java.text.ParseException;
import net.ubos.importer.facebook.AbstractFbJsonHandler;
import net.ubos.importer.facebook.FbHandlerContext;
import net.ubos.importer.facebook.FbUtils;
import net.ubos.mesh.MeshObject;
import net.ubos.mesh.MeshObjectIdentifier;
import net.ubos.meshbase.history.EditableHistoryMeshBase;
import net.ubos.model.Facebook.FacebookSubjectArea;
import net.ubos.model.ThreadedNotes.ThreadedNotesSubjectArea;
import net.ubos.model.primitives.StringValue;
import net.ubos.model.primitives.TimeStampValue;

public class YourPostsHandler
    extends
        AbstractFbJsonHandler
{
    /**
     * Constructor.
     *
     * @param insideZipFilePattern the path inside of the overall ZIP file that this importer matches
     */
    public YourPostsHandler(
            String insideZipFilePattern )
    {
        super( insideZipFilePattern );
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected double jsonReaderImport(
            JsonReader       jr,
            MeshObject       hereObject,
            FbHandlerContext context )
        throws
            ParseException,
            IOException
    {
        double ret = PERFECT;

        EditableHistoryMeshBase mb = context.getMeshBase();

//        mb.executeAt(   context.getTimeOfExport(),
//                        () -> {
                            mb.deleteMeshObject( hereObject );

                            jr.beginArray();
                            while( jr.hasNext() ) {
                                JsonObject currentPostJson = (JsonObject) JsonParser.parseReader( jr );

                                TimeStampValue timestamp = FbUtils.obtainTimestamp( currentPostJson.getAsJsonPrimitive( "timestamp" ));

                                MeshObjectIdentifier postObjId = mb.createMeshObjectIdentifierBelow(
                                        "posts",
                                        String.valueOf( timestamp.getAsMillis() ));
                                // some exist already because they are in albums or such

                                MeshObject postObj = mb.findMeshObjectByIdentifier( postObjId );
                                if( postObj == null ) {
                                    postObj = mb.createMeshObject(
                                            postObjId,
                                            FacebookSubjectArea.FACEBOOKPOST );

                                    postObj.setPropertyValue( FacebookSubjectArea.FACEBOOKPOST_CREATEDWHEN, timestamp );

                                    FbUtils.processDataPostLastUpdated( currentPostJson, postObj );
                                    FbUtils.processAttachments( currentPostJson, postObj );

                                    if( currentPostJson.has( "title" )) {
                                        String title = currentPostJson.getAsJsonPrimitive( "title" ).getAsString();
                                        postObj.setPropertyValue( ThreadedNotesSubjectArea.NOTE_SUBJECT, StringValue.createOrNull( title ));
                                    }
                                }
                                if( !postObj.isRelated( ThreadedNotesSubjectArea.NOTE_CREATEDBY_ACCOUNT_S, mb.getHomeObject() )) {
                                    postObj.blessRole( ThreadedNotesSubjectArea.NOTE_CREATEDBY_ACCOUNT_S, mb.getHomeObject() );
                                }
                            }
                            jr.endArray();
//                        });

        return ret;
    }
}
