//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.importer.facebook.on20200106.messages;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.google.gson.stream.JsonReader;
import java.io.IOException;
import java.text.ParseException;
import java.util.HashMap;
import java.util.Map;
import net.ubos.importer.facebook.AbstractFbJsonHandler;
import net.ubos.importer.facebook.FbHandlerContext;
import net.ubos.importer.facebook.FbUtils;
import net.ubos.importer.facebook.ObjectDirectory;
import net.ubos.importer.handler.UnexpectedContentException;
import net.ubos.mesh.MeshObject;
import net.ubos.meshbase.history.EditableHistoryMeshBase;
import net.ubos.model.Facebook.FacebookSubjectArea;
import net.ubos.model.ThreadedNotes.ThreadedNotesSubjectArea;
import net.ubos.model.primitives.EnumeratedValue;
import net.ubos.model.primitives.StringValue;
import net.ubos.model.primitives.TimeStampValue;

/**
 *
 */
public class MessagesHandler
    extends
        AbstractFbJsonHandler
{
    /**
     * Constructor.
     *
     * @param insideZipFilePattern the path inside of the overall ZIP file that this importer matches
     * @param messageThreadCategory type of message thread, derived from the directory
     */
    public MessagesHandler(
            String          insideZipFilePattern,
            EnumeratedValue messageThreadCategory )
    {
        super( insideZipFilePattern );

        theMessageThreadCategory = messageThreadCategory;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected final double jsonReaderImport(
            JsonReader       jr,
            MeshObject       hereObject,
            FbHandlerContext context )
        throws
            UnexpectedContentException,
            ParseException,
            IOException
    {
        EditableHistoryMeshBase mb  = context.getMeshBase();
        ObjectDirectory         dir = context.getObjectDirectory();

        JsonObject messageNodeJson = (JsonObject) JsonParser.parseReader( jr );
        JsonArray messagesJson = messageNodeJson.getAsJsonArray( "messages" );

        // find oldest message
        TimeStampValue oldestMessage = TimeStampValue.FUTUREST;

        for( int i=0 ; i<messagesJson.size() ; ++i ) {
            JsonObject      messageJson = (JsonObject)messagesJson.get( i );
            TimeStampValue  when        = FbUtils.obtainTimestamp( messageJson.getAsJsonPrimitive( "timestamp_ms" ));

            oldestMessage = TimeStampValue.earlierOf( oldestMessage, when );
        }

//        mb.executeAt(   oldestMessage.getAsMillis(),
//                        () -> {
                            String          title      = messageNodeJson.getAsJsonPrimitive( "title" ).getAsString();
                            EnumeratedValue threadType = determineMessageThreadType( messageNodeJson.getAsJsonPrimitive( "thread_type" ).getAsString() );

                            hereObject.bless( FacebookSubjectArea.FACEBOOKMESSAGETHREAD );

                            hereObject.setPropertyValue( FacebookSubjectArea.FACEBOOKMESSAGETHREAD_TITLE,    StringValue.createOrNull( title ));
                            hereObject.setPropertyValue( FacebookSubjectArea.FACEBOOKMESSAGETHREAD_TYPE,     threadType );
                            hereObject.setPropertyValue( FacebookSubjectArea.FACEBOOKMESSAGETHREAD_CATEGORY, theMessageThreadCategory );

                        // we skip this participants. This seems to be redundant information as it can be derived from the Messages themselves
//                        });

        for( int i=0 ; i<messagesJson.size() ; ++i ) {

            JsonObject      messageJson = (JsonObject)messagesJson.get( i );
            String          senderName  = messageJson.getAsJsonPrimitive( "sender_name" ).getAsString();
            TimeStampValue  when        = FbUtils.obtainTimestamp( messageJson.getAsJsonPrimitive( "timestamp_ms" ));
            EnumeratedValue type        = determineMessageType( messageJson.getAsJsonPrimitive( "type" ).getAsString() );

//            mb.executeAt(   when.getAsMillis(),
//                            () -> {
                                MeshObject messageObj = mb.createMeshObject(
                                        mb.createMeshObjectIdentifierBelow(
                                                hereObject.getIdentifier(),
                                                String.valueOf( when.getAsMillis() )),
                                        FacebookSubjectArea.FACEBOOKMESSAGE );
                                messageObj.setPropertyValue( FacebookSubjectArea.FACEBOOKMESSAGE_WHEN, when );
                                messageObj.setPropertyValue( FacebookSubjectArea.FACEBOOKMESSAGE_TYPE, type );

                                hereObject.blessRole( ThreadedNotesSubjectArea.DISCUSSION_COLLECTS_NOTE_S, messageObj );

                                if( senderName.length() > 1 ) {
                                    // apparently they can be empty
                                    MeshObject senderObj = dir.obtainByName( senderName, mb );
                                    messageObj.blessRole( ThreadedNotesSubjectArea.NOTE_CREATEDBY_ACCOUNT_S, senderObj );
                                }

                                if( messageJson.has( "content" )) {
                                    String          content     = messageJson.getAsJsonPrimitive( "content" ).getAsString();
                                    messageObj.setPropertyValue(
                                            ThreadedNotesSubjectArea.NOTE_CONTENT,
                                            ThreadedNotesSubjectArea.NOTE_CONTENT_type.createPlainTextBlobValue( content ));
                                }
                                if( messageJson.has( "sticker" )) {
                                    // FIXME
                                }
                                if( messageJson.has( "photos" )) {
                                    // FIXME
                                }
                                if( messageJson.has( "share" )) {
                                    // FIXME
                                }
//                          } );
        }
        return PERFECT;
    }

    /**
     * Type of message thread, derived from the directory.
     */
    protected final EnumeratedValue theMessageThreadCategory;


    protected static final Map<String,EnumeratedValue> MESSAGE_FOLDERS = new HashMap<>();
    static {
        MESSAGE_FOLDERS.put( "archived_threads", FacebookSubjectArea.FACEBOOKMESSAGETHREAD_CATEGORY_type_ARCHIVED );
        MESSAGE_FOLDERS.put( "filtered_threads", FacebookSubjectArea.FACEBOOKMESSAGETHREAD_CATEGORY_type_FILTERED );
        MESSAGE_FOLDERS.put( "inbox",            FacebookSubjectArea.FACEBOOKMESSAGETHREAD_CATEGORY_type_INBOX );
        MESSAGE_FOLDERS.put( "message_requests", FacebookSubjectArea.FACEBOOKMESSAGETHREAD_CATEGORY_type_REQUESTS );
    };

    protected EnumeratedValue determineMessageType(
            String value )
    {
        EnumeratedValue ret = MESSAGE_TYPES.get( value );
        return ret;
    }

    /**
     * Message types mapped to EnumeratedValues.
     */
    protected static final Map<String,EnumeratedValue> MESSAGE_TYPES = new HashMap<>();
    static {
        MESSAGE_TYPES.put( "Call",        FacebookSubjectArea.FACEBOOKMESSAGE_TYPE_type_CALL );
        MESSAGE_TYPES.put( "Generic",     FacebookSubjectArea.FACEBOOKMESSAGE_TYPE_type_GENERIC );
        MESSAGE_TYPES.put( "Payment",     FacebookSubjectArea.FACEBOOKMESSAGE_TYPE_type_PAYMENT );
        MESSAGE_TYPES.put( "Share",       FacebookSubjectArea.FACEBOOKMESSAGE_TYPE_type_SHARE );
        MESSAGE_TYPES.put( "Unsubscribe", FacebookSubjectArea.FACEBOOKMESSAGE_TYPE_type_UNSUBSCRIBE );
    }

    protected EnumeratedValue determineMessageThreadType(
            String value )
    {
        EnumeratedValue ret = MESSAGE_THREAD_TYPES.get( value );
        return ret;
    }

    /**
     * Message thread types mapped to EnumeratedValues.
     */
    protected static final Map<String,EnumeratedValue> MESSAGE_THREAD_TYPES = new HashMap<>();
    static {
        MESSAGE_THREAD_TYPES.put( "Pending",      FacebookSubjectArea.FACEBOOKMESSAGETHREAD_TYPE_type_PENDING );
        MESSAGE_THREAD_TYPES.put( "Regular",      FacebookSubjectArea.FACEBOOKMESSAGETHREAD_TYPE_type_REGULAR );
        MESSAGE_THREAD_TYPES.put( "RegularGroup", FacebookSubjectArea.FACEBOOKMESSAGETHREAD_TYPE_type_REGULARGROUP );
    }
}
