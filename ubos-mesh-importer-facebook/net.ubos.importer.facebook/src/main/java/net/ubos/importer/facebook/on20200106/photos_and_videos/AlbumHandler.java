//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.importer.facebook.on20200106.photos_and_videos;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.google.gson.stream.JsonReader;
import java.io.IOException;
import java.text.ParseException;
import net.ubos.importer.ImporterUtils;
import net.ubos.importer.facebook.AbstractFbJsonHandler;
import net.ubos.importer.facebook.FbHandlerContext;
import net.ubos.importer.facebook.FbUtils;
import net.ubos.importer.facebook.ObjectDirectory;
import net.ubos.importer.handler.UnexpectedContentException;
import net.ubos.mesh.BlessedAlreadyException;
import net.ubos.mesh.MeshObject;
import net.ubos.mesh.MeshObjectIdentifier;
import net.ubos.meshbase.history.EditableHistoryMeshBase;
import net.ubos.model.Blob.BlobSubjectArea;
import net.ubos.model.Facebook.FacebookSubjectArea;
import net.ubos.model.primitives.RoleType;
import net.ubos.model.primitives.StringValue;
import net.ubos.model.primitives.TimeStampValue;

/**
 *
 */
public class AlbumHandler
        extends
            AbstractFbJsonHandler
{
    /**
     * Constructor.
     *
     * @param insideZipFilePattern the path inside of the overall ZIP file that this importer matches
     */
    public AlbumHandler(
            String insideZipFilePattern )
    {
        super( insideZipFilePattern );
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected double jsonReaderImport(
            JsonReader       jr,
            MeshObject       hereObject,
            FbHandlerContext fbContext )
        throws
            UnexpectedContentException,
            ParseException,
            IOException
    {
        EditableHistoryMeshBase mb  = fbContext.getMeshBase();
        ObjectDirectory         dir = fbContext.getObjectDirectory();

        JsonObject albumJson  = (JsonObject) JsonParser.parseReader( jr );
        JsonArray  photosJson = albumJson.getAsJsonArray( "photos" );

        TimeStampValue oldestPhoto = FbUtils.obtainTimestamp( albumJson.getAsJsonObject( "cover_photo" ).getAsJsonPrimitive( "creation_timestamp" ));

        for( int i=0 ; i<photosJson.size() ; ++i ) {
            JsonObject photoJson = (JsonObject) photosJson.get( i );
            oldestPhoto = TimeStampValue.earlierOf( oldestPhoto, FbUtils.obtainTimestamp( photoJson.getAsJsonPrimitive( "creation_timestamp" )));
        }

//        MeshObject albumObj = mb.executeAt(
//                oldestPhoto.getAsMillis(),
//                ( tx ) -> {
                    mb.deleteMeshObject( hereObject );

                    String         albumName         = albumJson.getAsJsonPrimitive( "name" ).getAsString();
                    String         albumDescription  = albumJson.getAsJsonPrimitive( "description" ).getAsString();
                    TimeStampValue albumLastModified = FbUtils.obtainTimestamp( albumJson.getAsJsonPrimitive( "last_modified_timestamp" ));

                    MeshObject newAlbumObj = mb.createMeshObject(
                            mb.createMeshObjectIdentifierBelow( "albums", albumName ),
                            FacebookSubjectArea.FACEBOOKALBUM );

                    newAlbumObj.setPropertyValue( FacebookSubjectArea.FACEBOOKALBUM_NAME,            StringValue.createOrNull( albumName ));
                    newAlbumObj.setPropertyValue( FacebookSubjectArea.FACEBOOKALBUM_LASTUPDATEDWHEN, albumLastModified ); // FIXME is there a first modified, too?
                    newAlbumObj.setPropertyValue(
                            FacebookSubjectArea.FACEBOOKALBUM_DESCRIPTION,
                            FacebookSubjectArea.FACEBOOKALBUM_DESCRIPTION_type.createPlainTextBlobValue( albumDescription ));

                    JsonArray commentsJson = albumJson.getAsJsonArray( "comments" );
                    if( commentsJson != null ) {
                        FbUtils.handleMediaComments( commentsJson, newAlbumObj.getIdentifier(), dir, mb );
                    }

//                    return newAlbumObj;
//                });
MeshObject albumObj = newAlbumObj;

        handleOnePhoto(
                albumJson.getAsJsonObject( "cover_photo" ),
                albumObj.getIdentifier(),
                FacebookSubjectArea.FACEBOOKALBUM_HASCOVER_MEDIAOBJECT_S,
                fbContext );

        for( int i=0 ; i<photosJson.size() ; ++i ) {
            JsonObject photoJson = (JsonObject) photosJson.get( i );

            handleOnePhoto(
                    photoJson,
                    albumObj.getIdentifier(),
                    FacebookSubjectArea.FACEBOOKALBUM_COLLECTS_MEDIAOBJECT_S,
                    fbContext );
        }

        return PERFECT;
    }

    protected void handleOnePhoto(
            JsonObject           photoJson,
            MeshObjectIdentifier albumObjId,
            RoleType             albumToPhotoRoleType,
            FbHandlerContext     fbContext )
        throws
            ParseException,
            IOException
    {
        EditableHistoryMeshBase mb  = fbContext.getMeshBase();

        String         uri       = photoJson.getAsJsonPrimitive( "uri" ).getAsString();
        String         title     = photoJson.getAsJsonPrimitive( "title" ).getAsString();
        TimeStampValue createdOn = FbUtils.obtainTimestamp( photoJson.getAsJsonPrimitive( "creation_timestamp" ));

//        mb.executeAt(   createdOn.getAsMillis(),
//                        () -> {
                            MeshObject photoObj = ImporterUtils.findOrCreate(
                                    uri,
                                    BlobSubjectArea.PHOTO,
                                    mb );
                            MeshObject albumObj = mb.findMeshObjectByIdentifier( albumObjId );

                            photoObj.setPropertyValue( BlobSubjectArea.BLOBOBJECT_NAME, StringValue.create( title ));
                            photoObj.setPropertyValue( BlobSubjectArea.MEDIAOBJECT_CREATEDWHEN, createdOn );

                            try {
                                albumObj.blessRole( albumToPhotoRoleType, photoObj );

                            } catch( BlessedAlreadyException ex ) {
                                // might be, if the same photo is the cover photo
                            }
//                      } );
    }
}
