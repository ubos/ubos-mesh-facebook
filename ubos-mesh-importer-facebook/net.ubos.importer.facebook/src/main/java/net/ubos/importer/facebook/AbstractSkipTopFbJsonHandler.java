//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.importer.facebook;

import com.google.gson.stream.JsonReader;
import java.io.IOException;
import java.text.ParseException;
import net.ubos.importer.handler.UnexpectedContentException;
import net.ubos.mesh.MeshObject;

/**
 * Like AbstractFbJsonHandler, but reads a top-level object with one expected fields first before
 * delegating to the subclass.
 */
public abstract class AbstractSkipTopFbJsonHandler
    extends
        AbstractFbJsonHandler
{
    /**
     * Constructor.
     *
     * @param insideZipFilePattern the path inside of the overall ZIP file that this importer matches
     * @param expectedTopLevelElement the expected element in the top-level JsonObject that we enter
     */
    protected AbstractSkipTopFbJsonHandler(
            String  insideZipFilePattern,
            String  expectedTopLevelElement )
    {
        super( insideZipFilePattern );

        theExpectedTopLevelElement = expectedTopLevelElement;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected final double jsonReaderImport(
            JsonReader       jr,
            MeshObject       hereObject,
            FbHandlerContext context )
        throws
            UnexpectedContentException,
            ParseException,
            IOException
    {
        double ret = IMPOSSIBLE;

        jr.beginObject();
        while( jr.hasNext() ) {
            String name = jr.nextName();

            if( theExpectedTopLevelElement.equals( name )) {
                ret = jsonReaderImportBelow( jr, hereObject, context );

            } else {
                jr.skipValue();
            }
        }

        jr.endObject();
        return ret;
    }

    /**
     * Knows how to import JSON from a JsonReader.
     *
     * @param jr the to-be imported stream
     * @param hereObject the here MeshObject
     * @param context the context for the importing
     * @return the score
     * @throws UnexpectedContentException the ImporterHandler was not prepared for the content it found
     * @throws ParseException a parsing error occurred
     * @throws IOException an I/O project occurred
     */
    protected abstract double jsonReaderImportBelow(
            JsonReader       jr,
            MeshObject       hereObject,
            FbHandlerContext context )
        throws
            UnexpectedContentException,
            ParseException,
            IOException;

    /**
     * Name of the expected top-level elements.
     */
    protected final String theExpectedTopLevelElement;
}
