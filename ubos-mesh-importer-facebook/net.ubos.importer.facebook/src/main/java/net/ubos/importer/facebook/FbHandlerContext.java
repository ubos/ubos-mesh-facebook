//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.importer.facebook;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.zip.ZipFile;
import net.ubos.importer.handler.ImporterHandlerContext;
import net.ubos.meshbase.history.EditableHistoryMeshBase;
import net.ubos.model.primitives.BlobDataType;
import net.ubos.model.primitives.BlobValue;
import net.ubos.util.StreamUtils;

/**
 * Extends HandlerContext to carry more things we need for the Facebook import.
 */
public abstract class FbHandlerContext
    extends
        ImporterHandlerContext
{
    /**
     * Constructor.
     *
     * @param timeOfExport the time when the data source being imported was exported
     * @param mb the MeshBase
     * @param dir the ObjectDirectory
     */
    public FbHandlerContext(
            long                    timeOfExport,
            EditableHistoryMeshBase mb,
            ObjectDirectory         dir )
    {
        super( timeOfExport, mb );

        theObjectDirectory = dir;
    }

    public ObjectDirectory getObjectDirectory()
    {
        return theObjectDirectory;
    }

    public abstract BlobValue createBlobValueFrom(
            String       uri,
            BlobDataType dt )
        throws
            IOException;

    protected final ObjectDirectory theObjectDirectory;

    /**
     * Class to use when importing from a Zip file.
     */
    public static class Zip
        extends
            FbHandlerContext
    {
        /**
         * Constructor.
         *
         * @param timeOfExport the time when the data source being imported was exported
         * @param mb the MeshBase
         * @param dir the ObjectDirectory
         * @param zipFile the ZIP file that contains the Facebook data
         */
        public Zip(
                long                    timeOfExport,
                EditableHistoryMeshBase mb,
                ObjectDirectory         dir,
                ZipFile                 zipFile )
        {
            super( timeOfExport, mb, dir );

            theZipFile = zipFile;
        }

        public ZipFile getZipFile()
        {
            return theZipFile;
        }

        @Override
        public BlobValue createBlobValueFrom(
                String       path,
                BlobDataType dt )
            throws
                IOException
        {
            byte [] data = StreamUtils.slurp( theZipFile.getInputStream( theZipFile.getEntry( path ) ));
            String  mime = Files.probeContentType( Paths.get( path ));

            return dt.createBlobValue( data, mime );
        }

        protected final ZipFile theZipFile;
    }

    public static class Directory
        extends
            FbHandlerContext
    {
        /**
         * Constructor.
         *
         * @param timeOfExport the time when the data source being imported was exported
         * @param mb the MeshBase
         * @param dir the ObjectDirectory
         * @param file the File object that is the directory in the file system
         */
        public Directory(
                long                    timeOfExport,
                EditableHistoryMeshBase mb,
                ObjectDirectory         dir,
                File                    file )
        {
            super( timeOfExport, mb, dir );

            theFile = file;
        }

        @Override
        public BlobValue createBlobValueFrom(
                String       path,
                BlobDataType dt )
            throws
                IOException
        {
            File blobFile = new File( theFile, path );
            byte [] data = StreamUtils.slurp( blobFile );
            String  mime =  Files.probeContentType( Paths.get( path ));

            return dt.createBlobValue( data, mime );
        }

        protected final File theFile;
    }
}