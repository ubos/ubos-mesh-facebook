//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.importer.facebook.on20200106.following_and_followers;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.google.gson.stream.JsonReader;
import java.io.IOException;
import java.text.ParseException;
import net.ubos.importer.facebook.AbstractSkipTopFbJsonHandler;
import net.ubos.importer.facebook.FbHandlerContext;
import net.ubos.importer.facebook.ObjectDirectory;
import net.ubos.mesh.MeshObject;
import net.ubos.mesh.RoleTypeBlessedAlreadyException;
import net.ubos.meshbase.history.EditableHistoryMeshBase;
import net.ubos.model.Social.SocialSubjectArea;

public class FollowingHandler
    extends
        AbstractSkipTopFbJsonHandler
{
    /**
     * Constructor.
     *
     * @param insideZipFilePattern the path inside of the overall ZIP file that this importer matches
     * @param insideZipFilePattern the path inside of the overall ZIP file that this importer matches
     */
    public FollowingHandler(
            String insideZipFilePattern,
            String expectedTopLevelElement )
    {
        super( insideZipFilePattern, expectedTopLevelElement );
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected double jsonReaderImportBelow(
            JsonReader       jr,
            MeshObject       hereObject,
            FbHandlerContext context )
        throws
            ParseException,
            IOException
    {
        double ret = PERFECT;

        EditableHistoryMeshBase mb  = context.getMeshBase();
        ObjectDirectory         dir = context.getObjectDirectory();

        JsonArray data = (JsonArray) JsonParser.parseReader( jr );

//        mb.executeAt(   context.getTimeOfExport(),
//                        () -> {
//                            mb.deleteMeshObject( hereObject );

                            MeshObject home = mb.getHomeObject();

                            for( int i=0 ; i<data.size() ; ++i ) {
                                JsonObject current = (JsonObject) data.get( i );

                                String name = current.get( "name" ).getAsString();

                                MeshObject followingAccount = dir.obtainByName( name, mb );

                                try {
                                    home.blessRole( SocialSubjectArea.ACCOUNT_FOLLOWS_ACCOUNT_S, followingAccount );
                                } catch( RoleTypeBlessedAlreadyException ex ) {
                                    // two people with the same name -- cannot distinguish currently
                                    // see https://gitlab.com/accesstracker/data-access-issues/-/issues/33
                                }
                            }
//                        });

        return ret;
    }
}
