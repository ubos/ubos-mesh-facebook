//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.importer.facebook.on20200106.search_history;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.google.gson.stream.JsonReader;
import java.io.IOException;
import java.text.ParseException;
import net.ubos.importer.ImporterUtils;
import net.ubos.importer.facebook.AbstractSkipTopFbJsonHandler;
import net.ubos.importer.facebook.FbHandlerContext;
import net.ubos.importer.facebook.FbUtils;
import net.ubos.mesh.MeshObject;
import net.ubos.mesh.MeshObjectIdentifier;
import net.ubos.meshbase.history.EditableHistoryMeshBase;
import net.ubos.model.Facebook.FacebookSubjectArea;
import net.ubos.model.Search.SearchSubjectArea;
import net.ubos.model.primitives.StringValue;
import net.ubos.model.primitives.TimeStampValue;

public class YourSearchHistoryHandler
    extends
        AbstractSkipTopFbJsonHandler
{
    /**
     * Constructor.
     *
     * @param insideZipFilePattern the path inside of the overall ZIP file that this importer matches
     * @param expectedTopLevelElement the expected element in the top-level JsonObject that we enter
     */
    public YourSearchHistoryHandler(
            String insideZipFilePattern,
            String expectedTopLevelElement )
    {
        super( insideZipFilePattern, expectedTopLevelElement );
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected double jsonReaderImportBelow(
            JsonReader       jr,
            MeshObject       hereObject,
            FbHandlerContext context )
        throws
            ParseException,
            IOException
    {
        // the export has a whole structure there, but it doesn't seem to contain anything
        // other than the timestamp and the search string, with copy. So we ignore all this overhead.

        EditableHistoryMeshBase mb = context.getMeshBase();

//        mb.executeAt(   context.getTimeOfExport(),
//                        () -> {
                            mb.deleteMeshObject( hereObject );

                            jr.beginArray();

                            while( jr.hasNext() ) {
                                JsonObject search = (JsonObject) JsonParser.parseReader( jr );
                                TimeStampValue when = FbUtils.obtainTimestamp( search.get( "timestamp" ).getAsJsonPrimitive());

                                JsonArray data = (JsonArray) search.get( "data" );
                                if( data == null || data.size() != 1 ) {
                                    continue;
                                }
                                JsonElement text = ((JsonObject)data.get( 0 )).get( "text" );
                                if( text == null ) {
                                    continue;
                                }
                                String searchText = text.getAsString();

                                // Facebook claims that one can do lots of searches in the same second. So we append
                                // a counter if needed -- time resolution here is only seconds

                                MeshObjectIdentifier baseId = mb.createMeshObjectIdentifierBelow(
                                        "searches",
                                        String.valueOf( when.getAsMillis() ));

                                MeshObject searchQuery = ImporterUtils.createWithoutConflict( baseId, FacebookSubjectArea.FACEBOOKSEARCHQUERY, mb );

                                searchQuery.setPropertyValue( SearchSubjectArea.SEARCHQUERY_QUERY, StringValue.create( searchText ));
                                searchQuery.setPropertyValue( SearchSubjectArea.SEARCHQUERY_PERFORMEDWHEN, when );

                                mb.getHomeObject().blessRole( SearchSubjectArea.ACCOUNT_PERFORMS_SEARCHQUERY_S, searchQuery );
                            }

                            jr.endArray();
//                        });

        return theOkScore;
    }
}
