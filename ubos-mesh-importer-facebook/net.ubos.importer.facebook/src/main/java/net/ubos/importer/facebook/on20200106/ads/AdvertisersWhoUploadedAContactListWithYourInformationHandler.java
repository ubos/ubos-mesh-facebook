//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.importer.facebook.on20200106.ads;

import com.google.gson.JsonArray;
import com.google.gson.JsonParser;
import com.google.gson.stream.JsonReader;
import java.io.IOException;
import java.text.ParseException;
import net.ubos.importer.ImporterUtils;
import net.ubos.importer.facebook.AbstractSkipTopFbJsonHandler;
import net.ubos.importer.facebook.FbHandlerContext;
import net.ubos.importer.facebook.FbUtils;
import net.ubos.mesh.MeshObject;
import net.ubos.meshbase.history.EditableHistoryMeshBase;
import net.ubos.model.Facebook.FacebookSubjectArea;
import net.ubos.model.Marketing.MarketingSubjectArea;
import net.ubos.model.primitives.StringValue;

public class AdvertisersWhoUploadedAContactListWithYourInformationHandler
    extends
        AbstractSkipTopFbJsonHandler
{
    /**
     * Constructor.
     *
     * @param insideZipFilePattern the path inside of the overall ZIP file that this importer matches
     * @param expectedTopLevelElement the expected element in the top-level JsonObject that we enter
     */
    public AdvertisersWhoUploadedAContactListWithYourInformationHandler(
            String insideZipFilePattern,
            String expectedTopLevelElement )
    {
        super( insideZipFilePattern, expectedTopLevelElement );
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected double jsonReaderImportBelow(
            JsonReader       jr,
            MeshObject       hereObject,
            FbHandlerContext context )
        throws
            ParseException,
            IOException
    {
        double ret = PERFECT;

        EditableHistoryMeshBase mb = context.getMeshBase();

        JsonArray customAudiencesArray = (JsonArray) JsonParser.parseReader( jr );

//        mb.executeAt(   context.getTimeOfExport(),
//                        () -> {
                            mb.deleteMeshObject( hereObject );

                            for( int i=0 ; i<customAudiencesArray.size() ; ++i ) {
                                String customAudience = customAudiencesArray.get( i ).getAsString();

                                MeshObject advertiserObj = ImporterUtils.findOrCreate(
                                        FbUtils.identifierBelow( "advertisers", customAudience, mb ),
                                        FacebookSubjectArea.FACEBOOKADVERTISER,
                                        mb );
                                advertiserObj.setPropertyValue( MarketingSubjectArea.ADVERTISER_NAME, StringValue.create( customAudience ));

                                advertiserObj.blessRole( MarketingSubjectArea.ADVERTISER_BRINGSAUDIENCEINCLUDING_ACCOUNT_S, mb.getHomeObject() );
                            }
//                        });

        return ret;
    }
}
