//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.importer.facebook.on20210706;

import java.io.File;
import java.io.IOException;
import java.util.regex.Pattern;
import java.util.zip.ZipFile;
import net.ubos.importer.facebook.FbHandlerContext;
import net.ubos.importer.facebook.ObjectDirectory;
import net.ubos.importer.facebook.on20200106.about_you.FriendPeerGroupHandler;
import net.ubos.importer.facebook.on20200106.ads.AdsInterestsHandler;
import net.ubos.importer.facebook.on20200106.ads.AdvertisersWhoUploadedAContactListWithYourInformationHandler;
import net.ubos.importer.facebook.on20200106.ads.AdvertisersYouveInteractedWithHandler;
import net.ubos.importer.facebook.on20200106.apps_and_websites.AppsAndWebsitesHandler;
import net.ubos.importer.facebook.on20200106.apps_and_websites.YourAppsHandler;
import net.ubos.importer.facebook.on20200106.comments.CommentsHandler;
import net.ubos.importer.facebook.on20200106.following_and_followers.FollowersHandler;
import net.ubos.importer.facebook.on20200106.following_and_followers.FollowingHandler;
import net.ubos.importer.facebook.on20200106.friends.FriendsHandler;
import net.ubos.importer.facebook.on20200106.messages.MessagesHandler;
import net.ubos.importer.facebook.on20200106.photos_and_videos.AlbumHandler;
import net.ubos.importer.facebook.on20200106.photos_and_videos.FbTypedBlobImporterHandler;
import net.ubos.importer.facebook.on20200106.photos_and_videos.YourVideosHandler;
import net.ubos.importer.facebook.on20200106.posts.YourPostsHandler;
import net.ubos.importer.facebook.on20200106.profile_information.ProfileInformationHandler;
import net.ubos.importer.facebook.on20200106.saved_items_and_collections.SavedItemsAndCollectionsHandler;
import net.ubos.importer.facebook.on20200106.search_history.YourSearchHistoryHandler;
import net.ubos.importer.facebook.on20210706.apps_and_websites_off_of_facebook.YourOffFacebookActivityHandler;
import net.ubos.importer.handler.AbstractMultiHandlerImporter;
import net.ubos.importer.handler.ImporterHandler;
import net.ubos.importer.handler.ImporterHandlerContext;
import net.ubos.importer.handler.ImporterHandlerNode;
import net.ubos.importer.handler.directory.SkipIntermediateDirectoryImporterHandler;
import net.ubos.importer.handler.file.DefaultFileImporterHandler;
import net.ubos.importer.handler.json.DefaultJsonImporterHandler;
import net.ubos.importer.handler.remainders.RateRemaindersImporterHandler;
import net.ubos.importer.handler.zip.DefaultZipImporterHandler;
import net.ubos.mesh.namespace.MeshObjectIdentifierNamespace;
import net.ubos.meshbase.history.EditableHistoryMeshBase;
import net.ubos.model.Facebook.FacebookSubjectArea;
import net.ubos.util.logging.Log;

/**
 * Imports a Facebook data export ZIP file.
 */
public class FacebookImporter20210706
    extends
        AbstractMultiHandlerImporter
{
    private static final Log log = Log.getLogInstance(FacebookImporter20210706.class );

    /**
     * Constructor.
     */
    public FacebookImporter20210706()
    {
        super( "Facebook importer (format as of 2021-07-06)", HANDLERS, RATE_REMAINDERS_HANDLER );
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected ImporterHandlerContext setupImport(
            ImporterHandlerNode     rootNode,
            String                  defaultIdNamespace,
            EditableHistoryMeshBase mb )
        throws
            IOException
    {
        if( defaultIdNamespace != null ) {
            MeshObjectIdentifierNamespace thisNs = mb.getDefaultNamespace();
            thisNs.addExternalName( defaultIdNamespace );
            thisNs.setPreferredExternalName( defaultIdNamespace );
        }

        ObjectDirectory dir = new ObjectDirectory( mb );

        File topFile = rootNode.getFile();
        if( topFile.isDirectory() ) {
            return new FbHandlerContext.Directory( rootNode.getTimeOfExport(), mb, dir, topFile );

        } else {
            ZipFile zipFile = new ZipFile( topFile );
            return new FbHandlerContext.Zip( rootNode.getTimeOfExport(), mb, dir, zipFile );
        }
    }

    /**
     * Name of the file that contains the expected entry patterns.
     */
    public static final String EXPECTED_ENTRY_PATTERNS_FILE = "ExpectedEntryPatterns.txt";

    /**
     * The ImporterContentHandlers for this Importer
     */
    public static final ImporterHandler [] HANDLERS = {
        // Structure first
            new SkipIntermediateDirectoryImporterHandler(), // support unpacked directories, too
            new DefaultZipImporterHandler(),

            // we need information about ourselves first

            new ProfileInformationHandler(
                    "profile_information/profile_information.json", "profile_v2" ),

            // make sure the media objects are there when we look for them

            new FbTypedBlobImporterHandler(
                    "(messages|photos_and_videos)/.*\\.(gif|jpg|mp4|png)" ),
            new DefaultFileImporterHandler(
                    "messages/.*\\.(docx|pdf|txt)", ImporterHandler.SUFFICIENT ),

            new AdvertisersWhoUploadedAContactListWithYourInformationHandler(
                    "ads_information/advertisers_who_uploaded_a_contact_list_with_your_information.json", "custom_audiences_v2" ),
            new AdvertisersYouveInteractedWithHandler(
                    "ads_information/advertisers_you've_interacted_with.json", "history_v2" ),

            // first the apps, then the posts from them

            new AppsAndWebsitesHandler(
                    "apps_and_websites_off_of_facebook/apps_and_websites.json", "installed_apps_v2" ),
            new YourAppsHandler(
                    "apps_and_websites_off_of_facebook/your_apps.json", "admined_apps_v2" ),
            new YourOffFacebookActivityHandler(
                    "apps_and_websites_off_of_facebook/your_off-facebook_activity.json", "off_facebook_activity_v2" ),

            new CommentsHandler(
                    "comments_and_reactions/comments.json", "comments_v2" ),

            new FollowersHandler(
                    "friends_and_followers/people_who_follow_you.json", "followers_v2" ),
            new FollowingHandler(
                    "friends_and_followers/who_you_follow.json", "following_v2" ),

            new FriendsHandler(
                    "friends/friends.json", "friends_v2" ),

            new MessagesHandler(
                    "messages/archived_threads/.*/message_1.json", FacebookSubjectArea.FACEBOOKMESSAGETHREAD_CATEGORY_type_ARCHIVED ),
            new MessagesHandler(
                    "messages/filtered_threads/.*/message_1.json", FacebookSubjectArea.FACEBOOKMESSAGETHREAD_CATEGORY_type_FILTERED ),
            new MessagesHandler(
                    "messages/inbox/.*/message_1.json",            FacebookSubjectArea.FACEBOOKMESSAGETHREAD_CATEGORY_type_INBOX ),
            new MessagesHandler(
                    "messages/message_requests/.*/message_1.json", FacebookSubjectArea.FACEBOOKMESSAGETHREAD_CATEGORY_type_REQUESTS ),

            new AdsInterestsHandler(
                    "other_logged_information/ads_interests.json", "topics_v2" ),
            new FriendPeerGroupHandler(
                    "other_logged_information/friend_peer_group.json", "friend_peer_group_v2" ),

            new YourVideosHandler(
                    "posts/your_videos.json", "videos_v2" ),

            // no other_people's_post_to_your_timeline.json here

            new AlbumHandler(
                    "posts/album/\\d+.json" ),
            new YourPostsHandler(
                    "posts/your_posts_\\d+.json" ),

            new SavedItemsAndCollectionsHandler(
                    "saved_items_and_collections/saved_items_and_collections.json", "saves_and_collections_v2" ),

            new YourSearchHistoryHandler(
                    "search/your_search_history.json", "searches_v2" ),

    // Fallback

            new DefaultFileImporterHandler( ".*\\.(docx|gif|jpg|mp4|pdf|png|txt)", ImporterHandler.UNRATED ),
            new DefaultJsonImporterHandler( ImporterHandler.FALLBACK )
    };

    public static final RateRemaindersImporterHandler RATE_REMAINDERS_HANDLER;
    static {
        RateRemaindersImporterHandler handler = null;
        try {
            handler = new RateRemaindersImporterHandler(
                        FacebookImporter20210706.class.getResourceAsStream( EXPECTED_ENTRY_PATTERNS_FILE ),
                        ImporterHandler.SUFFICIENT, // we aren't actually parsing, but that's intentionally so
                        ImporterHandler.MAYBE_WRONG );

        } catch( IOException ex ) {
            log.error( ex );
        }
        RATE_REMAINDERS_HANDLER = handler;
    }

    /**
     * The filename pattern for a Facebook export file.
     */
    public static final Pattern FACEBOOK_EXPORT_FILENAME_PATTERN = Pattern.compile( "\\.zip$" );

}
