//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.importer.facebook.on20200106.comments;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.google.gson.stream.JsonReader;
import java.io.IOException;
import java.text.ParseException;
import java.util.regex.Pattern;
import net.ubos.importer.facebook.AbstractSkipTopFbJsonHandler;
import net.ubos.importer.facebook.FbHandlerContext;
import net.ubos.importer.facebook.FbUtils;
import net.ubos.importer.facebook.ObjectDirectory;
import net.ubos.importer.handler.UnexpectedContentException;
import net.ubos.mesh.MeshObject;
import net.ubos.mesh.MeshObjectIdentifier;
import net.ubos.meshbase.history.EditableHistoryMeshBase;
import net.ubos.model.Facebook.FacebookSubjectArea;
import net.ubos.model.ThreadedNotes.ThreadedNotesSubjectArea;
import net.ubos.model.primitives.StringValue;
import net.ubos.model.primitives.TimeStampValue;
import net.ubos.util.Pair;

public class CommentsHandler
    extends
        AbstractSkipTopFbJsonHandler
{
    /**
     * Constructor.
     *
     * @param insideZipFilePattern the path inside of the overall ZIP file that this importer matches
     * @param expectedTopLevelElement the expected element in the top-level JsonObject that we enter
     */
    public CommentsHandler(
            String insideZipFilePattern,
            String expectedTopLevelElement )
    {
        super( insideZipFilePattern, expectedTopLevelElement );
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected double jsonReaderImportBelow(
            JsonReader       jr,
            MeshObject       hereObject,
            FbHandlerContext context )
        throws
            UnexpectedContentException,
            ParseException,
            IOException
    {
        double ret = PERFECT;

        EditableHistoryMeshBase mb  = context.getMeshBase();
        ObjectDirectory         dir = context.getObjectDirectory();

//        mb.executeAt(   context.getTimeOfExport(),
//                        () -> {
                            mb.deleteMeshObject( hereObject );
//                        });

        JsonArray commentsArray = (JsonArray) JsonParser.parseReader( jr );

        for( int i=0 ; i<commentsArray.size() ; ++i ) {
            JsonObject currentComment = (JsonObject) commentsArray.get( i );

            String         title       = currentComment.get( "title" ).getAsString();
            TimeStampValue createdWhen = FbUtils.obtainTimestamp( currentComment.getAsJsonPrimitive( "timestamp" ));
            JsonArray      dataArray   = currentComment.getAsJsonArray( "data" );

//            mb.executeAt(   createdWhen.getAsMillis(),
//                            () -> {
                                Pair<MeshObject,MeshObject> senderReceiver = FbUtils.parseCommentTitle(
                                        title,
                                        COMMENT_OTHER_REGEXES,
                                        COMMENT_SELF_REGEXES,
                                        dir,
                                        mb );
                                MeshObject senderObj;
                                MeshObject receiverObj;
                                if( senderReceiver == null ) { // apparently that can happen per https://gitlab.com/ubos/ubos-mesh-facebook/-/issues/7
                                    senderObj   = null;
                                    receiverObj = null;
                                } else {
                                    senderObj   = senderReceiver.getName();
                                    receiverObj = senderReceiver.getValue();
                                }
                                if( senderObj == null ) {
//                                    return; // not sure what else to do
continue;
                                }

                                MeshObjectIdentifier commentObjId = mb.createMeshObjectIdentifierBelow(
                                                mb.createMeshObjectIdentifierBelow( senderObj.getIdentifier(), "comments" ),
                                                String.valueOf( createdWhen.getAsMillis() ));

                                MeshObject commentObj = mb.findMeshObjectByIdentifier( commentObjId );
                                if( commentObj != null ) {
                                    // must be a duplicate, see https://gitlab.com/accesstracker/data-access-issues/-/issues/32
//                                    return;
continue;
                                }

                                commentObj = mb.createMeshObject( commentObjId, FacebookSubjectArea.FACEBOOKCOMMENT );

                                commentObj.setPropertyValue( ThreadedNotesSubjectArea.NOTE_SUBJECT, StringValue.create( title ));
                                commentObj.setPropertyValue( FacebookSubjectArea.FACEBOOKCOMMENT_CREATEDWHEN, createdWhen );

                                if( dataArray != null ) {
                                    // apparently this may be null -- there is no payload
                                    if( dataArray.size() != 1 ) {
                                        throw new UnexpectedContentException( "Unexpected size of array: " + dataArray.size() );
                                    }
                                    if( dataArray.size() >= 1 ) {
                                        // deal with first element only
                                        JsonObject currentInsideComment = dataArray.get( 0 ).getAsJsonObject().getAsJsonObject( "comment" );

                                        String comment = currentInsideComment.get( "comment" ).getAsString();
                                        // we don't look at author: 1) not sure what to do with it, and 2) it's not always provided

                                        commentObj.setPropertyValue(
                                                ThreadedNotesSubjectArea.NOTE_CONTENT,
                                                ThreadedNotesSubjectArea.NOTE_CONTENT_type.createPlainTextBlobValue( comment ));

                                        FbUtils.processAttachments( currentComment, commentObj ); // only for some, like comments on external websites
                                    }
                                }
                                if( receiverObj != null ) {
                                    commentObj.blessRole( ThreadedNotesSubjectArea.NOTE_ISADDRESSEDTO_ACCOUNT_S, receiverObj );
                                }
                                commentObj.blessRole( ThreadedNotesSubjectArea.NOTE_CREATEDBY_ACCOUNT_S, senderObj );
//                          } );
        }
        return ret;
    }

    /**
     * Regex String that matches a name.
     */
    protected static final String NAME_REGEX_STRING = "(\\S.*\\S)";

    /**
     * A URL, not capturing.
     */
    protected static final String HOSTNAME_REGEX_STRING = "(?:[-a-z0-9]+\\.)+[-a-z0-9]+";

    /**
     * Regexes for the comment title formats we know that involve another person.
     */
    public static final Pattern [] COMMENT_OTHER_REGEXES = {
            Pattern.compile( NAME_REGEX_STRING + " replied to " + NAME_REGEX_STRING + "'s comment." ),
            Pattern.compile( NAME_REGEX_STRING + " commented on " + NAME_REGEX_STRING + "'s post." ),
            Pattern.compile( NAME_REGEX_STRING + " commented on " + NAME_REGEX_STRING + "'s photo." ),
            Pattern.compile( NAME_REGEX_STRING + " commented on " + NAME_REGEX_STRING + "'s video." ),
            Pattern.compile( NAME_REGEX_STRING + " commented on " + NAME_REGEX_STRING + "'s link." ),
            Pattern.compile( NAME_REGEX_STRING + " commented on " + NAME_REGEX_STRING + "'s life event." ),
            Pattern.compile( NAME_REGEX_STRING + " commented on " + HOSTNAME_REGEX_STRING + "." ),
            // those happen, too -- no payload either
            Pattern.compile( NAME_REGEX_STRING + " replied to a comment." ),
            Pattern.compile( NAME_REGEX_STRING + " commented on a post." ),
            Pattern.compile( NAME_REGEX_STRING + " commented on a photo." ),
            Pattern.compile( NAME_REGEX_STRING + " commented on a video." ),
            Pattern.compile( NAME_REGEX_STRING + " commented on a link." ),
            Pattern.compile( NAME_REGEX_STRING + " commented on a life event." )
    };

    /**
     * Regexes for the comment title formats we know that do not involve another person.
     */
    public static final Pattern [] COMMENT_SELF_REGEXES = {
            Pattern.compile( NAME_REGEX_STRING + " replied to his own comment." ),
            Pattern.compile( NAME_REGEX_STRING + " commented on his own post." ),
            Pattern.compile( NAME_REGEX_STRING + " commented on his own photo." ),
            Pattern.compile( NAME_REGEX_STRING + " commented on his own video." ),
            Pattern.compile( NAME_REGEX_STRING + " commented on his own link." ),
            Pattern.compile( NAME_REGEX_STRING + " commented on his own life event." )
    };
}
