//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.importer.facebook;

import java.text.ParseException;
import java.util.HashMap;
import net.ubos.mesh.MeshObject;
import net.ubos.mesh.MeshObjectIdentifier;
import net.ubos.mesh.namespace.MeshObjectIdentifierNamespace;
import net.ubos.meshbase.MeshBase;
import net.ubos.meshbase.history.EditableHistoryMeshBase;
import net.ubos.model.Facebook.FacebookSubjectArea;
import net.ubos.model.Identity.IdentitySubjectArea;
import net.ubos.model.primitives.StringValue;

/**
 * A directory between named objects (like friends) and the MeshObjectIdentifiers we create for them.
 */
public class ObjectDirectory
{
    /**
     * Constructor.
     *
     * @param mb the main MeshBase
     */
    public ObjectDirectory(
            EditableHistoryMeshBase mb )
    {
        theMeshBase = mb;
    }

    /**
     * Add the main user, so we don't get a facebook.com/user/mainuser and the Home Object instead
     *
     * @param key name of the main user
     */
    public void setMainUser(
            String key )
    {
        theDirectory.put( key, theMeshBase.getHomeMeshObjectIdentifier() );
    }

    /**
     * Smart factory method.
     *
     * @param name the name of the named object
     * @return the found or created MeshObjectIdentifier
     * @throws ParseException the identifier could not be parsed
     */
    public MeshObjectIdentifier obtainForUser(
            String name )
        throws
            ParseException
    {
        String realKey = "facebook.com/user/" + name;

        MeshObjectIdentifier ret = theDirectory.get( realKey );
        if( ret == null ) {
            MeshObjectIdentifierNamespace ns = theMeshBase.getPrimaryNamespaceMap().obtainByExternalName( realKey );

            ret = theMeshBase.getHomeObjectIdentifierIn( ns );

            theDirectory.put( realKey, ret );
        }
        return ret;
    }

    /**
     * Helper to find, or create, an Account MeshObject from a full name.
     *
     * @param fullName the full name
     * @param mb the MeshBase in which it shall be found
     * @return the Account MeshObject
     * @throws ParseException thrown if the name could not be parsed into a MeshObjectIdentifier
     */
    public MeshObject obtainByName(
            String   fullName,
            MeshBase mb )
        throws
            ParseException
    {
        MeshObjectIdentifier id = obtainForUser( fullName );

        MeshObject account = mb.findMeshObjectByIdentifier( id );
        if( account == null ) {
            account = mb.createMeshObject(
                    id,
                    FacebookSubjectArea.FACEBOOKACCOUNT );

            MeshObject newAccountProfile = mb.createMeshObject(
                    mb.createMeshObjectIdentifierBelow( id, "profile" ),
                    FacebookSubjectArea.FACEBOOKACCOUNTPROFILE );

            if( "Facebook User".equals( fullName )) {
                newAccountProfile.setPropertyValue(
                        IdentitySubjectArea.INDIVIDUALACCOUNTPROFILE_FULLNAME,
                        StringValue.create( "Facebook User (deleted or blocked)" ));
            } else {
                newAccountProfile.setPropertyValue(
                        IdentitySubjectArea.INDIVIDUALACCOUNTPROFILE_FULLNAME,
                        StringValue.create( fullName ));
            }

            newAccountProfile.blessRole( IdentitySubjectArea.ACCOUNTPROFILE_FOR_ACCOUNT_S, account );
        }
        return account;
    }

    /**
     * The MeshBase to use
     */
    protected final EditableHistoryMeshBase theMeshBase;

    /**
     * The underlying directory. This needs to map to identifier, not object, so we
     * get the MeshObject in the right MeshBaseView.
     */
    protected final HashMap<String,MeshObjectIdentifier> theDirectory = new HashMap<>();
}
