//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.importer.facebook.on20200106.photos_and_videos;

import net.ubos.importer.handler.blob.TypedBlobImporterHandler;

/**
 * Subclass this to keep the constructor parameters the same as the other Fb-specific ImporterHandlers
 * @author jernst
 */
public class FbTypedBlobImporterHandler
    extends
        TypedBlobImporterHandler
{
    public FbTypedBlobImporterHandler(
            String insideZipFilePattern )
    {
        super( "[^/]+/" + insideZipFilePattern, SUFFICIENT );
    }
}
