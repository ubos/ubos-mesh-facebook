//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.importer.facebook.on20200106.posts;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.google.gson.stream.JsonReader;
import java.io.IOException;
import java.text.ParseException;
import java.util.regex.Pattern;
import net.ubos.importer.facebook.AbstractSkipTop2FbJsonHandler;
import net.ubos.importer.facebook.FbHandlerContext;
import net.ubos.importer.facebook.FbUtils;
import net.ubos.importer.facebook.ObjectDirectory;
import net.ubos.mesh.MeshObject;
import net.ubos.meshbase.history.EditableHistoryMeshBase;
import net.ubos.model.Facebook.FacebookSubjectArea;
import net.ubos.model.ThreadedNotes.ThreadedNotesSubjectArea;
import net.ubos.model.primitives.StringValue;
import net.ubos.model.primitives.TimeStampValue;

public class OtherPeoplesPostsToYourTimelineHandler
    extends
        AbstractSkipTop2FbJsonHandler
{
    /**
     * Constructor.
     *
     * @param insideZipFilePattern the path inside of the overall ZIP file that this importer matches
     */
    public OtherPeoplesPostsToYourTimelineHandler(
            String insideZipFilePattern )
    {
        super( "wall_posts_sent_to_you", "activity_log_data", insideZipFilePattern );
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected double jsonReaderImportBelow2(
            JsonReader       jr,
            MeshObject       hereObject,
            FbHandlerContext context )
        throws
            ParseException,
            IOException
    {
        double ret = PERFECT;

        EditableHistoryMeshBase mb  = context.getMeshBase();
        ObjectDirectory         dir = context.getObjectDirectory();

//        mb.executeAt(   context.getTimeOfExport(),
//                        () -> {
                            mb.deleteMeshObject( hereObject );

                            JsonArray logDataJson = (JsonArray) JsonParser.parseReader( jr );

                            for( int i=0 ; i<logDataJson.size() ; ++i ) {
                                JsonObject logDataEntryJson = (JsonObject) logDataJson.get( i );

                                TimeStampValue timestamp = FbUtils.obtainTimestamp( logDataEntryJson.getAsJsonPrimitive( "timestamp" ));
                                String         title     = logDataEntryJson.getAsJsonPrimitive( "title" ).getAsString();

                                MeshObject sender = FbUtils.parseSentPostTitle( title, SENT_POST_REGEXES, dir, mb );

                                MeshObject postObj = mb.createMeshObject(
                                        mb.createMeshObjectIdentifierBelow(
                                                "posts",
                                                String.valueOf( timestamp.getAsMillis() )),
                                        FacebookSubjectArea.FACEBOOKPOST );

                                postObj.setPropertyValue( ThreadedNotesSubjectArea.NOTE_SUBJECT,        StringValue.createOrNull( title ));
                                postObj.setPropertyValue( FacebookSubjectArea.FACEBOOKPOST_CREATEDWHEN, timestamp );

                                FbUtils.processDataPostLastUpdated( logDataEntryJson, postObj );
                                FbUtils.processAttachments( logDataEntryJson, postObj );

                                postObj.blessRole( ThreadedNotesSubjectArea.NOTE_CREATEDBY_ACCOUNT_S, sender );
                                postObj.blessRole( ThreadedNotesSubjectArea.NOTE_ISADDRESSEDTO_ACCOUNT_S, mb.getHomeObject() );
                            }
//                        });

        return ret;
    }

    /**
     * Regex String that matches a name.
     */
    protected static final String NAME_REGEX_STRING = "(\\S.*\\S)";

    /**
     * Regexes for the title formats of posts sent to me.
     */
    public static final Pattern [] SENT_POST_REGEXES = {
            Pattern.compile( NAME_REGEX_STRING + " wrote on your timeline." ),
            Pattern.compile( NAME_REGEX_STRING + " added a new video to " + NAME_REGEX_STRING + "'s timeline." )
    };
}
