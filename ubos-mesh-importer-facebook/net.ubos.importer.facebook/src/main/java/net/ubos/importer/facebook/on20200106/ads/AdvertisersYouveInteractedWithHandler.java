//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.importer.facebook.on20200106.ads;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.google.gson.stream.JsonReader;
import java.io.IOException;
import java.text.ParseException;
import java.util.HashMap;
import java.util.Map;
import net.ubos.importer.ImporterUtils;
import net.ubos.importer.facebook.AbstractSkipTopFbJsonHandler;
import net.ubos.importer.facebook.FbHandlerContext;
import net.ubos.importer.facebook.FbUtils;
import net.ubos.mesh.MeshObject;
import net.ubos.meshbase.history.EditableHistoryMeshBase;
import net.ubos.model.Facebook.FacebookSubjectArea;
import net.ubos.model.Marketing.MarketingSubjectArea;
import net.ubos.model.primitives.EnumeratedValue;
import net.ubos.model.primitives.StringValue;
import net.ubos.model.primitives.TimeStampValue;

public class AdvertisersYouveInteractedWithHandler
    extends
        AbstractSkipTopFbJsonHandler
{
    /**
     * Constructor.
     *
     * @param insideZipFilePattern the path inside of the overall ZIP file that this importer matches
     * @param expectedTopLevelElement the expected element in the top-level JsonObject that we enter
     */
    public AdvertisersYouveInteractedWithHandler(
            String insideZipFilePattern,
            String expectedTopLevelElement )
    {
        super( insideZipFilePattern, expectedTopLevelElement );
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected double jsonReaderImportBelow(
            JsonReader       jr,
            MeshObject       hereObject,
            FbHandlerContext context )
        throws
            ParseException,
            IOException
    {
        double ret = PERFECT;

        EditableHistoryMeshBase mb = context.getMeshBase();

//        mb.executeAt(   context.getTimeOfExport(),
//                        () -> {
                            mb.deleteMeshObject( hereObject );
//                        });

        JsonArray historyArray = (JsonArray) JsonParser.parseReader( jr );

        for( int i=0 ; i<historyArray.size() ; ++i ) {
            JsonObject historyEntry = historyArray.get( i ).getAsJsonObject();

            String          title           = historyEntry.get( "title" ).getAsString();
            String          action          = historyEntry.get( "action" ).getAsString();
            TimeStampValue  when            = FbUtils.obtainTimestamp( historyEntry.get( "timestamp" ).getAsJsonPrimitive() );
            EnumeratedValue interactionType = determineInteractionType( action );

//            mb.executeAt(   when.getAsMillis(),
//                            () -> {
                                mb.deleteMeshObject( hereObject );

                                // note: the title may or may not be the name of the advertiser, see
                                // https://gitlab.com/accesstracker/data-access-issues/-/issues/18

                                MeshObject advertiserObj = ImporterUtils.findOrCreate(
                                        FbUtils.identifierBelow( "advertisers", title, mb ),
                                        FacebookSubjectArea.FACEBOOKADVERTISER,
                                        mb );
                                advertiserObj.setPropertyValue( MarketingSubjectArea.ADVERTISER_NAME, StringValue.create( title ));

                                MeshObject interactionObj = ImporterUtils.findOrCreate(
                                        FbUtils.identifierBelow( advertiserObj.getIdentifier().getLocalId(), String.valueOf( when.getAsMillis() ), mb ),
                                        FacebookSubjectArea.FACEBOOKADVERTISEMENTINTERACTION,
                                        mb );

                                interactionObj.setPropertyValue( MarketingSubjectArea.ADVERTISEMENTINTERACTION_TYPE, interactionType );
                                interactionObj.setPropertyValue( MarketingSubjectArea.ADVERTISEMENTINTERACTION_WHEN, when );
                                interactionObj.blessRole( MarketingSubjectArea.ADVERTISEMENTINTERACTION_WITHADVERTISEMENTBY_ADVERTISER_S, advertiserObj );
                                mb.getHomeObject().blessRole( MarketingSubjectArea.ACCOUNT_PERFORMS_ADVERTISEMENTINTERACTION_S, interactionObj );

                                if( !advertiserObj.isRelated( MarketingSubjectArea.ADVERTISER_BRINGSAUDIENCEINCLUDING_ACCOUNT_S, mb.getHomeObject())) {
                                    advertiserObj.blessRole( MarketingSubjectArea.ADVERTISER_BRINGSAUDIENCEINCLUDING_ACCOUNT_S, mb.getHomeObject() );
                                }
//                            });
        }

        return ret;
    }

    protected EnumeratedValue determineInteractionType(
            String value )
    {
        EnumeratedValue ret = INTERACTION_TYPES.get(  value );
        return ret;
    }


    /**
     * Interaction types mapped to EnumeratedValues.
     */
    protected static final Map<String,EnumeratedValue> INTERACTION_TYPES = new HashMap<>();
    static {
        INTERACTION_TYPES.put( "Clicked Ad", MarketingSubjectArea.ADVERTISEMENTINTERACTION_TYPE_type_CLICK );
    }

}
