//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.importer.facebook.on20200106.photos_and_videos;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.google.gson.stream.JsonReader;
import java.io.IOException;
import java.text.ParseException;
import net.ubos.importer.ImporterUtils;
import net.ubos.importer.facebook.AbstractSkipTopFbJsonHandler;
import net.ubos.importer.facebook.FbHandlerContext;
import net.ubos.importer.facebook.FbUtils;
import net.ubos.importer.facebook.ObjectDirectory;
import net.ubos.mesh.MeshObject;
import net.ubos.meshbase.history.EditableHistoryMeshBase;
import net.ubos.model.Blob.BlobSubjectArea;
import net.ubos.model.Facebook.FacebookSubjectArea;
import net.ubos.model.primitives.StringValue;
import net.ubos.model.primitives.TimeStampValue;

public class YourVideosHandler
    extends
        AbstractSkipTopFbJsonHandler
{
    /**
     * Constructor.
     *
     * @param insideZipFilePattern the path inside of the overall ZIP file that this importer matches
     * @param expectedTopLevelElement the expected element in the top-level JsonObject that we enter
     */
    public YourVideosHandler(
            String insideZipFilePattern,
            String expectedTopLevelElement )
    {
        super( insideZipFilePattern, expectedTopLevelElement );
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected double jsonReaderImportBelow(
            JsonReader       jr,
            MeshObject       hereObject,
            FbHandlerContext context )
        throws
            ParseException,
            IOException
    {
        double ret = PERFECT;

        EditableHistoryMeshBase mb  = context.getMeshBase();
        ObjectDirectory         dir = context.getObjectDirectory();

//        mb.executeAt(   context.getTimeOfExport(),
//                        () -> {
                            mb.deleteMeshObject( hereObject );

                            MeshObject videosObj = mb.createMeshObject(
                                    "videos",
                                    FacebookSubjectArea.FACEBOOKVIDEOSALBUM );
                            videosObj.setPropertyValue( FacebookSubjectArea.FACEBOOKALBUM_NAME, StringValue.create( "Videos" ));

                            JsonArray videosJson = (JsonArray) JsonParser.parseReader( jr );

                            for( int i=0 ; i<videosJson.size() ; ++i ) {
                                JsonObject videoJson = (JsonObject) videosJson.get( i );

                                String         uri               = videoJson.getAsJsonPrimitive( "uri" ).getAsString();
                                String         description       = videoJson.getAsJsonPrimitive( "description" ).getAsString();
                                TimeStampValue creationTimestamp = FbUtils.obtainTimestamp( videoJson.getAsJsonPrimitive( "creation_timestamp" ));
                                String         title;

                                if( videoJson.has( "title" )) {
                                    title = videoJson.getAsJsonPrimitive( "title" ).getAsString();
                                } else {
                                    title = null;
                                }

                                MeshObject videoObj = ImporterUtils.findOrCreate( // this should exist at this point
                                        uri, // this should probably be in its own namespace
                                        BlobSubjectArea.VIDEO,
                                        mb );

                                videoObj.setPropertyValue( BlobSubjectArea.MEDIAOBJECT_CREATEDWHEN, creationTimestamp );
                                videoObj.setPropertyValue( BlobSubjectArea.BLOBOBJECT_NAME,         StringValue.createOrNull( title ));
                                videoObj.setPropertyValue(
                                        BlobSubjectArea.MEDIAOBJECT_DESCRIPTION,
                                        BlobSubjectArea.MEDIAOBJECT_DESCRIPTION_type.createPlainTextBlobValue( description ));
                                videoObj.setPropertyValue(
                                        BlobSubjectArea.BLOBOBJECT_CONTENT,
                                        context.createBlobValueFrom( uri, BlobSubjectArea.BLOBOBJECT_CONTENT_type ));

                                JsonArray commentsJson = (JsonArray) videoJson.get( "comments" );
                                if( commentsJson != null ) {
                                    FbUtils.handleMediaComments( commentsJson, videoObj.getIdentifier(), dir, mb );
                                }
                                videosObj.blessRole( FacebookSubjectArea.FACEBOOKALBUM_COLLECTS_MEDIAOBJECT_S, videoObj );
                            }
//                        });

        return ret;
    }
}
