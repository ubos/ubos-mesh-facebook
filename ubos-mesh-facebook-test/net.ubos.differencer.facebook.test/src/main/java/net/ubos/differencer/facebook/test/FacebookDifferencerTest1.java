//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.differencer.facebook.test;

import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.Reader;
import java.nio.charset.StandardCharsets;
import java.util.List;
import net.ubos.mesh.namespace.m.MContextualMeshObjectIdentifierNamespaceMap;
import net.ubos.meshbase.MeshBaseDifferencer;
import net.ubos.meshbase.externalized.ImporterMeshObjectIdentifierDeserializer;
import net.ubos.meshbase.externalized.json.DefaultMeshBaseJsonImporter;
import net.ubos.meshbase.peertalk.externalized.json.PeerTalkJsonDecoder;
import net.ubos.meshbase.peertalk.externalized.json.PeerTalkJsonEncoder;
import net.ubos.meshbase.transaction.ChangeList;
import net.ubos.meshbase.transaction.externalized.ExternalizedChange;
import net.ubos.util.logging.Log;
import org.junit.jupiter.api.Test;

/**
 * Reads a .mesh file, and compares against empty MeshBase, serializes the ChangeList to disk,
 * and then applies it to a new MeshBase.
 */
public class FacebookDifferencerTest1
        extends
            AbstractFacebookDifferencerTest
{
    /**
     * Run the test.
     *
     * @throws Exception all sorts of things may go wrong during a test
     */
    @Test
    public void run()
        throws
            Exception
    {
        log.info( "Starting test " + getClass().getName() );

        //

        File meshFile    = new File( "/ubos/share/ubos-mesh-facebook-test/testfiles/FacebookDifferencerTest1-in.mesh" );
        File changesFile = new File( "/tmp/FacebookDifferencerTest1.changes" ); // not a complete PeerTalk file; no header

        if( changesFile.exists() ) {
            changesFile.delete();
        }

        //

        log.info( "Importing the file" );

        DefaultMeshBaseJsonImporter importer = DefaultMeshBaseJsonImporter.create();
        importer.importTo( meshFile, null, theMeshBase1 );

        //

        log.info( "Check some data has arrived" );

        checkCondition( theMeshBase1.size() > 1, "Wrong number of MeshObjects" );

        //

        log.info( "Run differencer against empty MeshBase as the baseline" );

        MeshBaseDifferencer diff2        = MeshBaseDifferencer.create( theMeshBase2 );
        ChangeList          changeList21 = diff2.determineChangeList( theMeshBase1 );

        //

        log.info( "Writing changes to disk" );

        PeerTalkJsonEncoder encoder = PeerTalkJsonEncoder.create();

        MContextualMeshObjectIdentifierNamespaceMap writingContextualNsMap
                = MContextualMeshObjectIdentifierNamespaceMap.create( thePrimaryNsMap );

        encoder.writePeerTalk( changeList21, true, true, writingContextualNsMap, new FileWriter( changesFile, StandardCharsets.UTF_8 ));

        //

        log.info( "Reading changes from disk and applying to empty MeshBase" );

        PeerTalkJsonDecoder decoder = PeerTalkJsonDecoder.create();

        MContextualMeshObjectIdentifierNamespaceMap reReadingContextualNsMap
                = MContextualMeshObjectIdentifierNamespaceMap.create( thePrimaryNsMap );

        ImporterMeshObjectIdentifierDeserializer idDeserializer = ImporterMeshObjectIdentifierDeserializer.create(
                theMeshBase2,
                reReadingContextualNsMap );

        try( Reader r = new FileReader( changesFile, StandardCharsets.UTF_8 )) {
            List<ExternalizedChange> changes = decoder.decodeHeadBody( r, idDeserializer );
            theMeshBase2.execute( () -> {
                for( ExternalizedChange change : changes ) {
                    change.applyTo( theMeshBase2 );
                }
            });
        }

        //

        log.assertLog( "Check there is no difference between original and new MeshBase" );

        MeshBaseDifferencer diffEnd       = MeshBaseDifferencer.create( theMeshBase1 );
        ChangeList          changeListEnd = diffEnd.determineChangeList( theMeshBase2 );

        checkEquals( changeListEnd.size(), 0, "There are changes, should be none" );
    }

    // Our Logger
    private static final Log log = Log.getLogInstance( FacebookDifferencerTest1.class );
}
