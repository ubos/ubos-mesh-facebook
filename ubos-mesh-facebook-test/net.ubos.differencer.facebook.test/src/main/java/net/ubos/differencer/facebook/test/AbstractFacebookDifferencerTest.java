//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.differencer.facebook.test;

import java.util.List;
import net.ubos.mesh.MeshObject;
import net.ubos.mesh.namespace.m.MPrimaryMeshObjectIdentifierNamespaceMap;
import net.ubos.meshbase.LoggingMeshBaseErrorListener;
import net.ubos.meshbase.MeshBase;
import net.ubos.meshbase.MeshBaseError;
import net.ubos.meshbase.m.MMeshBase;
import net.ubos.meshbase.transaction.Change;
import net.ubos.meshbase.transaction.ChangeList;
import net.ubos.testharness.AbstractTest;
import net.ubos.util.logging.Log;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;

/**
 * Factors out common functionality for FacebookDifferencerTests.
 */
public abstract class AbstractFacebookDifferencerTest
        extends
            AbstractTest
{
    /**
     * Set up.
     *
     * @throws Exception all sorts of things may go wrong in a test
     */
    @BeforeEach
    public void setup()
        throws
            Exception
    {
        thePrimaryNsMap = MPrimaryMeshObjectIdentifierNamespaceMap.create();

        theMeshBase1 = MMeshBase.Builder.create().namespaceMap(thePrimaryNsMap ).build();
        theMeshBase2 = MMeshBase.Builder.create().namespaceMap(thePrimaryNsMap ).build();

        LoggingMeshBaseErrorListener listener1 = new LoggingMeshBaseErrorListener() {
            @Override
            protected void logError(
                    MeshBaseError error )
            {
                Assertions.fail( "MeshBase 1 reported an error: " + error );
            }
        };
        LoggingMeshBaseErrorListener listener2 = new LoggingMeshBaseErrorListener() {
            @Override
            protected void logError(
                    MeshBaseError error )
            {
                Assertions.fail( "MeshBase 2 reported an error: " + error );
            }
        };
        theMeshBase1.addDirectErrorListener( listener1 );
        theMeshBase2.addDirectErrorListener( listener2 );
    }

    /**
     * Helper to print the contents of a MeshBase.
     *
     * @param useThis the Log to print to
     * @param theMeshBase the MeshBase to print
     */
    protected void printMeshBase(
            Log      useThis,
            MeshBase theMeshBase )
    {
        if( useThis.isDebugEnabled() ) {
            useThis.debug( "MeshBase content: " + theMeshBase.size() + " MeshObjects" );

            int i=0;
            for( MeshObject current : theMeshBase ) {
                useThis.debug( " " + i + ": " + current );
                ++i;
            }
        }
    }

    /**
     * Helper to print a ChangeList.
     *
     * @param useThis the Log to print to
     * @param changeList the ChangeList to print
     */
    protected void printChangeList(
            Log        useThis,
            ChangeList changeList )
    {
        if( useThis.isDebugEnabled() ) {
            List<Change> changes = changeList.getChanges();

            useThis.debug( "found " + changes.size() + " changes" );

            for( int i=0 ; i<changes.size() ; ++i ) {
                Change current = changes.get( i );
                useThis.debug( " " + i + ": " + current );
            }
        }
    }

    /**
     * Clean up after the test.
     */
    @AfterEach
    public void cleanup()
    {
        theMeshBase2.die();
        theMeshBase1.die();
    }

    /**
     * The primary namespace map.
     */
    protected MPrimaryMeshObjectIdentifierNamespaceMap thePrimaryNsMap;

    /**
     * The first MeshBase for the test.
     */
    protected MMeshBase theMeshBase1;

    /**
     * The second MeshBase for the test.
     */
    protected MMeshBase theMeshBase2;
}
