//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.importer.facebook.encoding.test;

import net.ubos.importer.facebook.FbUtils;
import net.ubos.testharness.AbstractTest;
import net.ubos.util.logging.Log;
import org.junit.jupiter.api.Test;

/**
 * Tests the decoding of Facebook's invalid UTF-8 encoding.
 */
public class Utf8DecodingTest1
        extends
            AbstractTest
{
    /**
     * Run the test.
     *
     * @throws Exception all sorts of things may go wrong during a test
     */
    @Test
    public void run()
        throws
            Exception
    {
        log.info( "Starting test " + getClass().getName() );

        for( int i=0 ; i<TESTDATA.length ; ++i ) {
            String raw     = TESTDATA[i][0];
            String correct = TESTDATA[i][1];

            String fixed20200106 = FbUtils.fixBrokenCharEncoding( raw );

            checkEquals( fixed20200106, correct, "Wrong in FacebookParser20200106, index: " + i );
            noop();
        }
    }
    public void noop() {}

    public static final String [][] TESTDATA = {
        { "\\u00c2\\u00ab", "\u00ab" },
        // { "\\u00c2\\u00b0", "" },
        // { "\\u00c2\\u00ba", "" },
        { "\\u00c3\\u0084", "\u00c4" },
        { "\\u00c3\\u0087", "\u00c7" },
        { "\\u00c3\\u0089", "\u00c9" },
        { "\\u00c3\\u0096fter mal was neues! Feiere trotzdem so sch\\u00c3\\u00b6n wie m\\u00c3\\u00b6glich.", "\u00d6fter mal was neues! Feiere trotzdem so sch\u00f6n wie m\u00f6glich." },
        { "\\u00c3\\u0098", "\u00d8" },
        { "\\u00c3\\u009cbrigens in N\\u00c3\\u00bcrnberg", "\u00dcbrigens in N\u00fcrnberg" },
        { "\\u00c3\\u009f", "\u00df" },
        { "\\u00c3\\u00a0", "\u00e0" },
        { "\\u00c3\\u00a3", "\u00e3" },
        { "\\u00c3\\u00a4hnlicher", "\u00e4hnlicher" },
        { "\\u00c3\\u00ad", "\u00ed" },
        { "\\u00c3\\u00b1", "\u00f1" },
        { "h\\u00c3\\u00b6flich", "h\u00f6flich" },
        { "\\u00c3\\u00b8", "\u00f8" },
        { "F\\u00c3\\u00bcr zwei", "F\u00fcr zwei" },

// FAILS:
//        { "\\u00d0\\u0098\\u00d1\\u0080\\u00d0\\u00b8\\u00d0\\u00bd\\u00d0\\u00b0 \\u00d0\\u0091\\u00d0\\u00be\\u00d1\\u0080\\u00d0\\u00b8\\u00d1\\u0081\\u00d0\\u00be\\u00d0\\u00b2\\u00d0\\u00bd\\u00d0\\u00b0",
//          "????? ?????????" },

// FAILS:
//        { "\\u00e0\\u00b8\\u0099.\\u00e0\\u00b8\\u00aa \\u00e0\\u00b8\\u0088\\u00e0\\u00b8\\u00b2\\u00e0\\u00b8\\u00a3\\u00e0\\u00b8\\u00b4\\u00e0\\u00b8\\u00a7\\u00e0\\u00b8\\u00a3\\u00e0\\u00b8\\u00a3\\u00e0\\u00b8\\u0093 \\u00e0\\u00b8\\u009a\\u00e0\\u00b8\\u00b8\\u00e0\\u00b8\\u008d\\u00e0\\u00b8\\u00a2\\u00e0\\u00b8\\u00b7\\u00e0\\u00b8\\u0099",
//          "\"?.? ???????? ??????\"" },

// FAILS:
//        { "\\u00e1\\u00bb\\u00a9c kho\\u00e1\\u00ba\\u00bb C\\u00e1\\u00bb\\u0099ng \\u00c4\\u0091\\u00e1\\u00bb\\u0093",
//          "?c kho? C?ng ??" },

// FAILS:
//        { "\\u00e2\\u0080\\u008e\\u00d9\\u0085\\u00d9\\u0086\\u00d8\\u00b5\\u00d8\\u00a9 \\u00d9\\u0085\\u00d8\\u00ad\\u00d8\\u00a7\\u00d8\\u00b1\\u00d8\\u00a8\\u00d8\\u00a9 \\u00d9\\u0083\\u00d9\\u0088\\u00d8\\u00b1\\u00d9\\u0088\\u00d9\\u0086\\u00d8\\u00a7 - \\u00d8\\u00a7\\u00d9\\u0084\\u00d8\\u00b3\\u00d9\\u0088\\u00d8\\u00af\\u00d8\\u00a7\\u00d9\\u0086\\u00e2\\u0080\\u008e",
//          "????? ?????? ?????? - ????????" },
        { "\\u00e2\\u0080\\u0093", "\u2013" }, // ndash
        { "\\u00e2\\u0080\\u0094", "\u2014" }, // mdash
        { "\\u00e2\\u0080\\u0098", "\u2018" }, // left quotation mark
        { "\\u00e2\\u0080\\u0098four stories\\u00e2\\u0080\\u0099", "\u2018four stories\u2019" },
        { "\\u00e2\\u0080\\u009cConservatives retweeted Russian trolls about 31 times more often than liberals.\\u00e2\\u0080\\u009d",
          "\u201cConservatives retweeted Russian trolls about 31 times more often than liberals.\u201d" },
        // { "\\u00e2\\u0080\\u00aaLook what I bought today. \\u00e2\\u0080\\u00ac", "" }, // left to right embedding??!?
//        { "\\u00e2\\u0084\\u00a2", "" },
//        { "\\u00e2\\u0085\\u0094", "" },
//        { "\\u00e2\\u0089\\u00a0", "" },
//        { "\\u00e2\\u0098\\u00b0\\n\\u00e2\\u0086\\u0099", "" },
//        { "\\u00e2\\u0099\\u00a1", "" },
        { "\\u00e2\\u0099\\u00a5\\u00ef\\u00b8\\u008f", "\u2665\ufe0f" }, // black heart with an "emoji variation selector"
//        { "\\u00e2\\u009c\\u0085", "" },
//        { "\\u00e2\\u009d\\u00a4", "" },
//        { "\\u00e2\\u009d\\u00a4\\u00ef\\u00b8\\u008f", "" },
//        { "\\u00e2\\u009d\\u00a4\\u00ef\\u00b8\\u008f and \\u00e2\\u0098\\u00ae", "" },
//        { "\\u00e3\\u0083\\u0086\\u00e3\\u0082\\u00b0\\u00e3\\u0083\\u00ac\\u00e3\\u0083\\u0083\\u00e3\\u0083\\u0088\\u00e6\\u008a\\u0080\\u00e8\\u00a1\\u0093\\u00e9\\u0096\\u008b\\u00e7\\u0099\\u00ba, Name \\u00e9\\u0087\\u008e\\u00e6\\u0089\\u008b\\u00e6\\u00ad\\u00a3\\u00e4\\u00b9\\u008b\\nIf you do not mind, I'd like to take \\u00e7\\u0089\\u0087\\u00e6\\u00a1\\u0090\\u00e9\\u0087\\u008f", "" },
//        { "\\u00e3\\u0083\\u00aa\\u00e3\\u0083\\u00b3\\u00e3\\u0082\\u00ab\\u00e6\\u00a0\\u00aa\\u00e5\\u00bc\\u008f\\u00e4\\u00bc\\u009a\\u00e7\\u00a4\\u00be", "" },
//        { "\\u00e5\\u0088\\u0086\\u00e6\\u0095\\u00a3\\u00e3\\u0082\\u00b3\\u00e3\\u0083\\u00b3\\u00e3\\u0083\\u0094\\u00e3\\u0083\\u00a5\\u00e3\\u0083\\u00bc\\u00e3\\u0083\\u0086\\u00e3\\u0082\\u00a3\\u00e3\\u0083\\u00b3\\u00e3\\u0082\\u00b0\\u00e6\\u008e\\u00a8\\u00e9\\u0080\\u00b2\\u00e5\\u008d\\u0094\\u00e4\\u00bc\\u009a", "" },
//        { "\\u00e5\\u008d\\u0081\\u00e4\\u00b8\\u0089\\u00e9\\u009f\\u00b3\\u00e4\\u00bc\\u009a", "" },
//        { "\\u00e5\\u0091\\u0082\\u00e6\\u0098\\u00ad\\u00e6\\u00af\\u0085", "" },
//        { "\\u00e5\\u00b0\\u008f\\u00e6\\u0099\\u0082\\u00e6\\u0089\\u0093\\u00e9\\u0080\\u00a0\\u00e5\\u00b0\\u0088\\u00e5\\u00b1\\u00ac\\u00e8\\u0087\\u00aa\\u00e5\\u00b7\\u00b1\\u00e7\\u009a\\u0084\\u00e7\\u00b6\\u00b2\\u00e8\\u00b7\\u00af\\u00e5\\u008d\\u00b0\\u00e9\\u0088\\u0094\\u00e6\\u00a9\\u009f", "" },
//        { "\\u00e6\\u00a0\\u00aa\\u00e5\\u00bc\\u008f\\u00e4\\u00bc\\u009a\\u00e7\\u00a4\\u00be\\u00e9\\u009b\\u00bb\\u00e9\\u0080\\u009a\\u00e3\\u0083\\u0087\\u00e3\\u0082\\u00b8\\u00e3\\u0082\\u00bf\\u00e3\\u0083\\u00ab", "" },


// This is code point 1F642, which is outside of the \u0000 range and needs to be encoded with a UTF-16 "surrogate pair".
// Hack upon hack. https://en.wikipedia.org/wiki/JSON#Character_encoding
//         { "\\u00e9\\u009f\\u00a9\\u00e4\\u00b8\\u00bd\\u00e4\\u00b8\\u00bd", "" },

//        { "\\u00ef\\u00bb\\u00bf", "" },
//        { "\\u00f0\\u009f\\u008e\\u0081", "" },
//        { "\\u00f0\\u009f\\u008e\\u0084\\u00f0\\u009f\\u008e\\u0081", "" },
//        { "\\u00f0\\u009f\\u0091\\u008e", "" },
//        { "\\u00f0\\u009f\\u0091\\u00b9", "" },
//        { "\\u00f0\\u009f\\u0098\\u0081", "" },
//        { "\\u00f0\\u009f\\u0098\\u0082", "" },
//        { "\\u00f0\\u009f\\u0098\\u0086", "" },
//        { "\\u00f0\\u009f\\u0098\\u0087", "" },
//        { "\\u00f0\\u009f\\u0098\\u008a", "" },
//        { "\\u00f0\\u009f\\u0098\\u008d", "" },
//        { "\\u00f0\\u009f\\u0098\\u008d\\u00f0\\u009f\\u0098\\u008d\\u00f0\\u009f\\u0098\\u008d\\u00f0\\u009f\\u0098\\u008d\\u00f0\\u009f\\u0098\\u008d\\u00f0\\u009f\\u0098\\u008d", "" },
//        { "\\u00f0\\u009f\\u0098\\u0092", "" },
//        { "\\u00f0\\u009f\\u0098\\u009b", "" },
//        { "\\u00f0\\u009f\\u0098\\u009b isn\\u00e2\\u0080\\u0099", "" },
//        { "\\u00f0\\u009f\\u0098\\u009e", "" },
//        { "\\u00f0\\u009f\\u0098\\u00a2", "" },
//        { "\\u00f0\\u009f\\u0098\\u00ae", "" },
//        { "\\u00f0\\u009f\\u0098\\u00b1", "" },
//        { "\\u00f0\\u009f\\u0099\\u0082", "" },
//        { "\\u00f0\\u009f\\u0099\\u0082 ... and I\\u00e2\\u0080\\u0099m sure there are some. Just because somebody made their professors happy enough some years ago doesn\\u00e2\\u0080\\u0099", "" },
//        { "\\u00f0\\u009f\\u0099\\u0082 And I would add, also a \\u00e2\\u0080\\u009ctransparency regime\\u00e2\\u0080\\u009d", "" },
//        { "\\u00f0\\u009f\\u0099\\u0082 viele viele Gl\\u00c3\\u00bcckw\\u00c3\\u00bcnsche und W\\u00c3\\u00bcnsche f\\u00c3\\u00bc", "" },
//        { "\\u00f0\\u009f\\u0099\\u008c\\u00f0\\u009f\\u008f\\u00bd", "" },
//        { "\\u00f0\\u009f\\u0099\\u008f\\u00f0\\u009f\\u008f\\u00be", "" },
//        { "\\u00f0\\u009f\\u00a4\\u00a3\\u00f0\\u009f\\u00a4\\u00a3\\u00f0\\u009f\\u00a4\\u00a3\\u00f0\\u009f\\u00a4\\u00a3\\u00f0\\u009f\\u0098\\u0082\\u00f0\\u009f\\u0098\\u0082\\u00f0\\u009f\\u0098\\u0082\\u00f0\\u009f\\u0098\\u0082", "" },
//        { "\\u00f0\\u009f\\u00a5\\u00b3\\nHope you\\u00e2\\u0080\\u0099", "" },
//        { "\\u00f3\\u00be\\u008c\\u00b5", "" },
//        { "\\u00f3\\u00be\\u00ae\\u009f\\u00f3\\u00be\\u00ae\\u009f\\u00f3\\u00be\\u00ae\\u009f\\u00f3\\u00be\\u00ae\\u009f\\u00f3\\u00be\\u00ae\\u009f\\u00f3\\u00be\\u00ae\\u009f", "" }
    };

    private static final Log log = Log.getLogInstance( Utf8DecodingTest1.class );
}
